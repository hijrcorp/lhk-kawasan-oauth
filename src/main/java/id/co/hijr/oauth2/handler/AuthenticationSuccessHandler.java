package id.co.hijr.oauth2.handler;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.util.LinkedMultiValueMap;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.WebUtils;
import id.co.hijr.oauth2.mapper.AccountLoginMapper;
import id.co.hijr.oauth2.mapper.LoginMapper;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.Source;
import id.co.hijr.oauth2.security.TwoFactorAuthenticationInterceptor;
import id.co.hijr.oauth2.services.AuditService;
import id.co.hijr.oauth2.services.HijrTokenService;
import id.co.hijr.oauth2.services.PerangkatService;
import id.co.hijr.oauth2.services.UserService;


public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Value("${app.cookie.name}")
	private String cookieName;
	
	@Value("${app.cookie.used}")
	protected String cookieUsed;
	
	@Value("${security.oauth2.client.id}")
	private String clientId;
	
	@Value("${security.oauth2.resource.id}")
	private String resourceId;
	
	@Value("${app.url.ntfc}")
	private String urlNtfc = "";
	
	@Autowired
    private AuthorizationServerTokenServices tokenServices;
	
	@Autowired
    private UserService userService;
	
	@Autowired
	private PerangkatService perangkatService;
	
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		if(cookieUsed.equals("true")) {
			String accessTokenValue = WebUtils.getCookieTokenValue(request, cookieName);
			HijrTokenService ts  = (HijrTokenService)tokenServices;
			if(ts.readAccessToken(accessTokenValue) == null || accessTokenValue.equals("")) {
				OAuth2AccessToken token = generateOauth2AccessToken(authentication);
				accessTokenValue = token.getValue();
				Cookie sessionCookie = new Cookie( cookieName, token.getValue() );
		        sessionCookie.setMaxAge(token.getExpiresIn());
		        response.addCookie( sessionCookie );
		        
			}	
			
			/*
			if (WebUtils.hasRole(authentication.getAuthorities(), TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED)) {
				userService.sendOtp(accessTokenValue, UserService.OTP_TRANSPORT_EMAIL);
			}*/
			
			perangkatService.register(request, accessTokenValue);
		}
		
		super.onAuthenticationSuccess(request, response, authentication);
	}
	
    private OAuth2AccessToken generateOauth2AccessToken(Authentication authentication) {
    		AccountLogin user = (AccountLogin) authentication.getPrincipal();

        boolean approved = true;

        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();

        Map<String, String> requestParameters = new HashMap<String, String>();
        Map<String, Serializable> extensionProperties = new HashMap<String, Serializable>();
        Set<String> scope = new HashSet<>();
        scope.add(resourceId);

        Set<String> resourceIds = new HashSet<>();
        Set<String> responseTypes = new HashSet<>();
        responseTypes.add("code");

        OAuth2Request oAuth2request = new OAuth2Request(requestParameters, clientId, authorities, approved, scope, resourceIds, null, responseTypes, extensionProperties);

        OAuth2Authentication auth = new OAuth2Authentication(oAuth2request, authentication);

        HijrTokenService ts  = (HijrTokenService)tokenServices;
        return ts.createAccessToken(auth);
    }
	
}
