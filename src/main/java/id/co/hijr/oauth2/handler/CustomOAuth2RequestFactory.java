package id.co.hijr.oauth2.handler;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import id.co.hijr.oauth2.security.TwoFactorAuthenticationInterceptor;

public class CustomOAuth2RequestFactory extends DefaultOAuth2RequestFactory {
	public static final String SAVED_AUTHORIZATION_REQUEST_SESSION_ATTRIBUTE_NAME = "savedAuthorizationRequest";
	public static final String SAVED_STATE_REQUEST_SESSION_ATTRIBUTE_NAME = "savedStateRequest";

    public CustomOAuth2RequestFactory(ClientDetailsService clientDetailsService) {
        super(clientDetailsService);
    }

    @Override
    public AuthorizationRequest createAuthorizationRequest(Map<String, String> authorizationParameters) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession(false);
        if (session != null) {
            AuthorizationRequest authorizationRequest = (AuthorizationRequest) session.getAttribute(SAVED_AUTHORIZATION_REQUEST_SESSION_ATTRIBUTE_NAME);
            String state = (String)session.getAttribute(SAVED_STATE_REQUEST_SESSION_ATTRIBUTE_NAME);
            System.out.println( state + " -- lewaaat...." + authorizationRequest);
            if (authorizationRequest != null) {
            	
            		if(state != null && state.equals(TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATED)) {
            			System.out.println(" buaang....");
                     session.removeAttribute(SAVED_AUTHORIZATION_REQUEST_SESSION_ATTRIBUTE_NAME);
                     session.removeAttribute(SAVED_STATE_REQUEST_SESSION_ATTRIBUTE_NAME);
            		}
            		
                
                return authorizationRequest;
            }
            
            
        }

        return super.createAuthorizationRequest(authorizationParameters);
    }
}
