package id.co.hijr.oauth2;

import java.util.Collection;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.co.hijr.oauth2.model.Token;


public class WebUtils {
	

	public static String getCookieTokenValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();

		String accessTokenValue = "";
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
			  String name = cookies[i].getName();
			  String value = cookies[i].getValue();
			  //System.out.println(name + "--" + value);
			  if(name.equals(cookieName)){
				  accessTokenValue= value;
			  }
			}
		}
		
		return accessTokenValue;
	}
	
	 public static void setCookie(HttpServletRequest request,HttpServletResponse response, String cookieName, Token token) {
 		Cookie accessTokenCookie = new Cookie(cookieName, token.getAccessToken());
 		accessTokenCookie.setMaxAge(token.getExpiresIn().intValue()); 
		accessTokenCookie.setPath("/");
		//System.out.println("host: " + request.getServerName());
		if(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")) {
			accessTokenCookie.setDomain(request.getServerName());
		}else {
			accessTokenCookie.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
		}
		
		response.addCookie(accessTokenCookie);
		
//		Cookie refreshTokenCookie = new Cookie("oauth2-refresh-token", token.getRefreshToken());
//		refreshTokenCookie.setMaxAge(60*60*24*365); //Store cookie for 1 year
//		refreshTokenCookie.setPath("/");
//		refreshTokenCookie.setDomain(".hijr.co.id");
//		response.addCookie(refreshTokenCookie);
 }
	 
	 public static boolean hasRole(Collection roles, String role) {
			
			if(roles == null) return false;
			
			Object[] arr = roles.toArray();
			
			for(Object o: arr) {
				if(o.toString().equals(role)) {
					return true;
				}
			}
			
			return false;
		}
	
}
