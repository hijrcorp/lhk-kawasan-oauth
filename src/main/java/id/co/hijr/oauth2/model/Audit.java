package id.co.hijr.oauth2.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Audit {
	public static final String ID = "id_audit";
	public static final String DATE = "date_audit";
	public static final String TYPE = "type_audit";
	public static final String CODE = "code_audit";
	public static final String DETAILS = "details_audit";
	public static final String ID_SOURCE = "id_source_audit";
	public static final String ACCOUNT_ADDED = "account_added_audit";
	public static final String TIMESTAMP_ADDED = "timestamp_added_audit";

	private String id;
	private Date date;
	private String type;
	private String code;
	private String details;
	private String idSource;
	private String accountAdded;
	private Date timestampAdded;

	public Audit() {

	}

	public Audit(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("date")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("details")
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	@JsonProperty("account_added")
	public String getAccountAdded() {
		return accountAdded;
	}

	public void setAccountAdded(String accountAdded) {
		this.accountAdded = accountAdded;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}



	/**********************************************************************/

}
