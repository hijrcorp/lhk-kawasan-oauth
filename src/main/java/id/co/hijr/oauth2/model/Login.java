package id.co.hijr.oauth2.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Login {

	
	public static final String ID = "id_login";
	public static final String ACCESS_TOKEN = "access_token_login";
	public static final String CLIENT_ID = "client_id_login";
	public static final String USERNAME = "username_login";
	public static final String REFRESH_TOKEN = "refresh_token_login";
	public static final String CREATED_TIME = "created_time_login";
	public static final String EXPIRE_TIME = "expire_time_login";
	public static final String TOKEN_OBJECT = "token_object_login";
	public static final String ACCOUNT_ID = "account_id_login";
	public static final String SOURCE_ID = "source_id_login";
	public static final String MFA_CODE = "mfa_code_login";
	public static final String MFA_CODE_EXPIRED = "mfa_code_expired_login";

	private String id;
	private String accessToken;
	private String clientId;
	private String username;
	private String refreshToken;
	private Date createdTime;
	private Date expireTime;
	private byte[] tokenObject;
	private String accountId;
	private String sourceId;
	private String mfaCode;
	private Date mfaCodeExpired;

	public Login() {

	}

	public Login(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("access_token")
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@JsonProperty("client_id")
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@JsonProperty("username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("refresh_token")
	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@JsonProperty("created_time")
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@JsonProperty("expire_time")
	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	@JsonProperty("token_object")
	public byte[] getTokenObject() {
		return tokenObject;
	}

	public void setTokenObject(byte[] tokenObject) {
		this.tokenObject = tokenObject;
	}

	@JsonProperty("account_id")
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	/**********************************************************************/
	
	public static final String REFRESH = "REFRESH";
	public static final String LOGOUT = "LOGOUT";
	public static final String EXPIRED = "EXPIRED";
	
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("source_id")
	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getMfaCode() {
		return mfaCode;
	}

	public void setMfaCode(String mfaCode) {
		this.mfaCode = mfaCode;
	}

	public Date getMfaCodeExpired() {
		return mfaCodeExpired;
	}

	public void setMfaCodeExpired(Date mfaCodeExpired) {
		this.mfaCodeExpired = mfaCodeExpired;
	}


}
