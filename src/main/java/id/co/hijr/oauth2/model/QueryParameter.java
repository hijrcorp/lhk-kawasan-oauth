package id.co.hijr.oauth2.model;

public class QueryParameter {
	
	private String clause = "1";
	private String values = "";
	private String order;
	private Integer limit = 100;
	private Integer offset = 0;
	
	public QueryParameter() {
		// TODO Auto-generated constructor stub
	}

	public String getClause() {
		return clause;
	}

	public void setClause(String clause) {
		this.clause = clause;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	
}
