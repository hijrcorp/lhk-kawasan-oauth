package id.co.hijr.oauth2.model;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountLogin extends Account implements UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3960871218791119694L;
	
	public static final String ID = "id_account";
	public static final String USERNAME = "username_account";
	public static final String PASSWORD = "password_account";
	public static final String EMAIL = "email_account";
	public static final String FULL_NAME = "full_name_account";
	public static final String MOBILE = "mobile_account";
	public static final String ENABLED = "enabled_account";
	public static final String ACCOUNT_NON_EXPIRED = "account_non_expired_account";
	public static final String CREDENTIALS_NON_EXPIRED = "credentials_non_expired_account";
	public static final String ACCOUNT_NON_LOCKED = "account_non_locked_account";
	public static final String USING_MFA = "using_mfa_account";
	public static final String MFA_USING_WA = "mfa_using_wa_account";
	public static final String USING_PIN = "using_pin_account";
	public static final String NOTIFIKASI_PERANGKAT = "notifikasi_perangkat_account";
	public static final String WAJIB_VERIFIKASI = "wajib_verifikasi_account";

	private String id;
	private String username;
	private String password;
	private String email;
	private String fullName;
	private String mobile;
	private boolean enabled;
	private boolean accountNonExpired;
	private boolean credentialsNonExpired;
	private boolean accountNonLocked;
	private Boolean usingMfa;
	private Boolean mfaUsingWa;
	private Boolean usingPin;
	private Boolean notifikasiPerangkat;
	private Boolean wajibVerifikasi;

	public AccountLogin() {

	}

	public AccountLogin(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("full_name")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonProperty("mobile")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@JsonProperty("enabled")
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@JsonProperty("account_non_expired")
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	@JsonProperty("credentials_non_expired")
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	@JsonProperty("account_non_locked")
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	@JsonProperty("using_mfa")
	public Boolean isUsingMfa() {
		return usingMfa;
	}

	public void setUsingMfa(Boolean usingMfa) {
		this.usingMfa = usingMfa;
	}

	@JsonProperty("mfa_using_wa")
	public Boolean isMfaUsingWa() {
		return mfaUsingWa;
	}

	public void setMfaUsingWa(Boolean mfaUsingWa) {
		this.mfaUsingWa = mfaUsingWa;
	}

	@JsonProperty("using_pin")
	public Boolean isUsingPin() {
		return usingPin;
	}

	public void setUsingPin(Boolean usingPin) {
		this.usingPin = usingPin;
	}

	@JsonProperty("notifikasi_perangkat")
	public Boolean isNotifikasiPerangkat() {
		return notifikasiPerangkat;
	}

	public void setNotifikasiPerangkat(Boolean notifikasiPerangkat) {
		this.notifikasiPerangkat = notifikasiPerangkat;
	}

	@JsonProperty("wajib_verifikasi")
	public Boolean isWajibVerifikasi() {
		return wajibVerifikasi;
	}

	public void setWajibVerifikasi(Boolean wajibVerifikasi) {
		this.wajibVerifikasi = wajibVerifikasi;
	}



	/**********************************************************************/

	
	private Collection<? extends GrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	
	
	public final static String ADMIN = "admin_account";
	public final static String VERIFIKATOR = "verifikator_account";
	
	private Source source;
	private boolean admin;
	private boolean verifikator;

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public boolean isVerifikator() {
		return verifikator;
	}

	public void setVerifikator(boolean verifikator) {
		this.verifikator = verifikator;
	}



}
