package id.co.hijr.oauth2.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Reset {
	public static final String ID = "id_reset";
	public static final String TYPE = "type_reset";
	public static final String EXPIRED = "expired_reset";
	public static final String ACCOUNT = "account_reset";
	public static final String CODE = "code_reset";
	public static final String IS_OPEN = "is_open_reset";

	private String id;
	private String type;
	private Date expired;
	private String account;
	private String code;
	private Boolean open;

	public Reset() {

	}

	public Reset(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("expired")
	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@JsonProperty("account")
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("is_open")
	public Boolean isOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}



	/**********************************************************************/

}
