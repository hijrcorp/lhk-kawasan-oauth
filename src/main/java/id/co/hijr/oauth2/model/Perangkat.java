package id.co.hijr.oauth2.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Perangkat {
	public static final String ID = "id_perangkat";
	public static final String KODE = "kode_perangkat";
	public static final String NAMA = "nama_perangkat";
	public static final String TIPE = "tipe_perangkat";
	public static final String TOKEN = "token_perangkat";
	public static final String JENIS_TOKEN = "jenis_token_perangkat";
	public static final String IS_TERVERIFIKASI = "is_terverifikasi_perangkat";
	public static final String ID_SOURCE = "id_source_perangkat";
	public static final String ID_ACCOUNT = "id_account_perangkat";
	public static final String ACCOUNT_ADDED = "account_added_perangkat";
	public static final String TIMESTAMP_ADDED = "timestamp_added_perangkat";
	public static final String ACCOUNT_MODIFIED = "account_modified_perangkat";
	public static final String TIMESTAMP_MODIFIED = "timestamp_modified_perangkat";

	private String id;
	private String kode;
	private String nama;
	private String tipe;
	private String token;
	private String jenisToken;
	private Boolean terverifikasi;
	private String idSource;
	private String idAccount;
	private String accountAdded;
	private Date timestampAdded;
	private String accountModified;
	private Date timestampModified;

	public Perangkat() {

	}

	public Perangkat(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("kode")
	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("tipe")
	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	@JsonProperty("token")
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@JsonProperty("jenis_token")
	public String getJenisToken() {
		return jenisToken;
	}

	public void setJenisToken(String jenisToken) {
		this.jenisToken = jenisToken;
	}

	@JsonProperty("is_terverifikasi")
	public Boolean isTerverifikasi() {
		return terverifikasi;
	}

	public void setTerverifikasi(Boolean terverifikasi) {
		this.terverifikasi = terverifikasi;
	}

	@JsonProperty("id_source")
	public String getIdSource() {
		return idSource;
	}

	public void setIdSource(String idSource) {
		this.idSource = idSource;
	}

	@JsonProperty("id_account")
	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	@JsonProperty("account_added")
	public String getAccountAdded() {
		return accountAdded;
	}

	public void setAccountAdded(String accountAdded) {
		this.accountAdded = accountAdded;
	}

	@JsonProperty("timestamp_added")
	public Date getTimestampAdded() {
		return timestampAdded;
	}

	public void setTimestampAdded(Date timestampAdded) {
		this.timestampAdded = timestampAdded;
	}

	@JsonProperty("account_modified")
	public String getAccountModified() {
		return accountModified;
	}

	public void setAccountModified(String accountModified) {
		this.accountModified = accountModified;
	}

	@JsonProperty("timestamp_modified")
	public Date getTimestampModified() {
		return timestampModified;
	}

	public void setTimestampModified(Date timestampModified) {
		this.timestampModified = timestampModified;
	}



	/**********************************************************************/


}
