package id.co.hijr.oauth2.model;

import java.io.Serializable;

public class Source implements Serializable {
	
	public final static String ID = "id_source";
	public final static String NAME = "name_source";
	
	private String id;
	private String name;
	
	public Source() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
