package id.co.hijr.oauth2.template;

import java.util.Collections;
import java.util.List;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

public class BearerAuthRestTemplate extends RestTemplate {

	private String accessToken;

	public BearerAuthRestTemplate(String accessToken) {
		super();
		this.accessToken = accessToken;
		addAuthentication();

	}

	private void addAuthentication() {
		if (StringUtils.isEmpty(accessToken)) {
			throw new RuntimeException("Token is mandatory for Bearer Auth");
		}

		List<ClientHttpRequestInterceptor> interceptors = Collections
				.singletonList(new BearerAuthInterceptor(accessToken));
		setRequestFactory(new InterceptingClientHttpRequestFactory(getRequestFactory(), interceptors));
	}
}