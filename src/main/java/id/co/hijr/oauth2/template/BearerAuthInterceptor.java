package id.co.hijr.oauth2.template;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class BearerAuthInterceptor implements ClientHttpRequestInterceptor {

	private String accessToken;

	public BearerAuthInterceptor(String accessToken) {
		this.accessToken = accessToken;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes,
			ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
		HttpHeaders headers = httpRequest.getHeaders();
		headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);

		return clientHttpRequestExecution.execute(httpRequest, bytes);
	}

}
