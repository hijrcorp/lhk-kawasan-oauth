package id.co.hijr.oauth2.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.mapper.AccountLoginMapper;
import id.co.hijr.oauth2.mapper.LoginMapper;
import id.co.hijr.oauth2.mapper.PerangkatMapper;
import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.Perangkat;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.Source;

@Service
public class PerangkatService {
	
	@Value("${app.url.ntfc}")
	private String urlNtfc = "";

	@Value("${ntfc.app.code}")
	private String code = "";
	
	@Value("${ntfc.app.config}")
	private String config = "";
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private final String COOKIE_NAME = "DEVICE_ID";
	
	public String getDeviceId(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();

		String devid = "";
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String name = cookies[i].getName();
				String value = cookies[i].getValue();
				if (name.equals(COOKIE_NAME)) {
					devid = value;
				}
			}
		}

		return devid;
	}
	
	@Autowired
    private LoginMapper loginMapper;
	
	@Autowired
    private PerangkatMapper perangkatMapper;
	
	@Autowired
    private AccountLoginMapper accountLoginMapper;
	
	@Autowired
    private AuditService auditService;
	
	public void register(HttpServletRequest request, String accessToken, String devid, String tipe, String nama) {
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessToken+"'");
//		param.setClause(param.getClause() + " AND " + Login.EXPIRE_TIME + " > NOW()");
//		param.setClause(param.getClause() + " AND " + Login.MFA_CODE_EXPIRED + " > NOW()");
		
		List<Login> lstLogin = loginMapper.getList(param);
		if(lstLogin.size() > 0) {
			Login login = lstLogin.get(0);
			
			QueryParameter paramAcc = new QueryParameter();
			paramAcc.setClause(paramAcc.getClause() + " AND " + Source.ID + "='"+login.getSourceId()+"'");
			paramAcc.setClause(paramAcc.getClause() + " AND " + AccountLogin.ID + "='"+login.getAccountId()+"'");
			List<AccountLogin> lst = accountLoginMapper.getList(paramAcc);
			
			if(lst.size() > 0) {
				AccountLogin account = lst.get(0);
				
				register(request, account, account.getSource().getId(), devid, tipe, nama);
			}
			
		}
	}
	
	public void register(HttpServletRequest request, String accessToken) {
		
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessToken+"'");
//		param.setClause(param.getClause() + " AND " + Login.EXPIRE_TIME + " > NOW()");
//		param.setClause(param.getClause() + " AND " + Login.MFA_CODE_EXPIRED + " > NOW()");
		
		List<Login> lstLogin = loginMapper.getList(param);
		if(lstLogin.size() > 0) {
			Login login = lstLogin.get(0);
			
			QueryParameter paramAcc = new QueryParameter();
			paramAcc.setClause(paramAcc.getClause() + " AND " + Source.ID + "='"+login.getSourceId()+"'");
			paramAcc.setClause(paramAcc.getClause() + " AND " + AccountLogin.ID + "='"+login.getAccountId()+"'");
			List<AccountLogin> lst = accountLoginMapper.getList(paramAcc);
			
			if(lst.size() > 0) {
				AccountLogin account = lst.get(0);
				
				register(request, account, account.getSource().getId());
			}
			
		}
		
	}
	
	public void register(HttpServletRequest request, Account account, String sourceId) {
		String devid = getDeviceId(request);
		

		Map<String, String> map = getBrowserInfo(request);
		
		String tipe = map.get("browser").toString();
		String nama = map.get("details").toString();
		
		register(request, account, sourceId, devid, tipe, nama);
		
	}
	
	public void register(HttpServletRequest request, Account account, String sourceId, String devid, String tipe, String nama) {
		if(devid.equals(""))	 return;	
		
		QueryParameter paramDev = new QueryParameter();
		paramDev.setClause(paramDev.getClause() + " AND " + Perangkat.KODE + "='"+devid+"'");
		paramDev.setClause(paramDev.getClause() + " AND " + Perangkat.ID_ACCOUNT + "='"+account.getId()+"'");
		
		List<Perangkat> list = perangkatMapper.getList(paramDev);
		
		
		if(list.size() == 0) {
			Perangkat perangkat = new Perangkat(Utils.getUUIDString());
			perangkat.setKode(devid);
			perangkat.setNama(nama);
			perangkat.setTipe(tipe);
			perangkat.setIdAccount(account.getId());
			perangkat.setAccountAdded(account.getFullName());
			perangkat.setTimestampAdded(new Date());
			perangkat.setIdSource(sourceId);
			
			perangkatMapper.insert(perangkat);
			
			sendEmail(account.getEmail(), perangkat, sourceId);
			
			auditService.add(AuditService.AUDIT_TYPE_DEVICE+"_NEW", AuditService.AUDIT_TYPE_DEVICE, "Tipe: "+tipe+", Info: "+ nama, account.getId(), sourceId);
			
		}else {
			Perangkat perangkat = list.get(0);
			
			perangkat.setAccountModified(account.getFullName());
			perangkat.setTimestampModified(new Date());
			
			perangkatMapper.update(perangkat);
			
			auditService.add(AuditService.AUDIT_TYPE_DEVICE+"_UPDATE", AuditService.AUDIT_TYPE_DEVICE, "Tipe: "+tipe+", Info: "+ nama, account.getId(), sourceId);
			
		}
		
	}
	
	public void updateToken(HttpServletRequest request, String accessToken,  String devid, String jenis, String token) {

		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessToken+"'");
//		param.setClause(param.getClause() + " AND " + Login.EXPIRE_TIME + " > NOW()");
//		param.setClause(param.getClause() + " AND " + Login.MFA_CODE_EXPIRED + " > NOW()");
		
		List<Login> lstLogin = loginMapper.getList(param);
		if(lstLogin.size() > 0) {
			Login login = lstLogin.get(0);
			
			QueryParameter paramAcc = new QueryParameter();
			paramAcc.setClause(paramAcc.getClause() + " AND " + Source.ID + "='"+login.getSourceId()+"'");
			paramAcc.setClause(paramAcc.getClause() + " AND " + AccountLogin.ID + "='"+login.getAccountId()+"'");
			List<AccountLogin> lst = accountLoginMapper.getList(paramAcc);
			
			if(lst.size() > 0) {
				AccountLogin account = lst.get(0);
				
				QueryParameter paramDev = new QueryParameter();
				paramDev.setClause(paramDev.getClause() + " AND " + Perangkat.KODE + "='"+devid+"'");
				paramDev.setClause(paramDev.getClause() + " AND " + Perangkat.ID_ACCOUNT + "='"+account.getId()+"'");
				
				List<Perangkat> list = perangkatMapper.getList(paramDev);
				
				
				if(list.size() > 0) {
					Perangkat perangkat = list.get(0);
					
					perangkat.setAccountModified(perangkat.getAccountAdded());
					perangkat.setTimestampModified(new Date());
					
					perangkat.setJenisToken(jenis);
					perangkat.setToken(token);
					
					perangkatMapper.update(perangkat);
				}
			}
			
		}
		
	}
	
	public void generate(HttpServletRequest request, HttpServletResponse response) {
		
		if(getDeviceId(request).equals("")) {
			
			Cookie sessionCookie = new Cookie( COOKIE_NAME, Utils.getUUIDString());
			int exp = 24*3600*365*10;
	        sessionCookie.setMaxAge(exp);
	        response.addCookie( sessionCookie );
			
		}
		
		
	}
	
	public Map<String, String> getBrowserInfo(HttpServletRequest request) {
		Map<String, String> map = new HashMap<>();
		
		String  browserDetails  =   request.getHeader("User-Agent");
        String  userAgent       =   browserDetails;
        String  user            =   userAgent.toLowerCase();

        String os = "";
        String browser = "";

        logger.info("User Agent for the request is===>"+browserDetails);
        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE-" + user.substring(user.indexOf("rv") + 3, user.indexOf(")"));
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        logger.info("Operating System======>"+os);
        logger.info("Browser Name==========>"+browser);
        
        map.put("os", os);
        map.put("browser", browser);
        map.put("details", browserDetails);
        
        
        return map;
	}
	
	 public void sendEmail(String email, Perangkat perangkat, String sourceId) {
// 		System.out.println(">> SENT: " + email);
 	
		Map<String, Object> payload = new HashMap<String, Object>();
//		payload.add("email", email);
//		payload.add("subject", "Kode OTP Verifikasi Login");
//		payload.add("body", "<p>Kode OTP anda adalah <strong>"+kode+"</strong> dan hanya berlaku dalam waktu 10 menit.</p><p>Ini adalah kode <strong>RAHASIA</strong>, harap jangan berikan kepada siapapun.</p>");
		
		
		payload.put("notification_title", "Notifikasi Perangkat Baru");
		payload.put("notification_body_text", "<p>Sistem kami mendeteksi adanya aktivitas yang menggunakan email anda pada perangkat berikut ini:</p><p><strong>"+perangkat.getTipe()+": </strong>"+perangkat.getNama()+"</p>");
		

		payload.put("notification_app", code);
		payload.put("notification_settings", config);
		
		payload.put("notification_user_added", sourceId);
		payload.put("email_recipients", "EMAIL_SINGLE_TO:"+email);
		
		String urlNotificationSave = urlNtfc+"/notification/save";
//		String urlNotificationSave = urlNtfc+"/send/email";
		
		Utils.postRestForm(payload, urlNotificationSave, new RestTemplate());
	}
}
