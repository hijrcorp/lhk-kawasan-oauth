package id.co.hijr.oauth2.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.mapper.AccountLoginMapper;
import id.co.hijr.oauth2.mapper.AccountMapper;
import id.co.hijr.oauth2.mapper.ControlMapper;
import id.co.hijr.oauth2.mapper.LoginMapper;
import id.co.hijr.oauth2.mapper.ResetMapper;
import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.Control;
import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.Reset;
import id.co.hijr.oauth2.model.Source;
import id.co.hijr.oauth2.security.TwoFactorAuthenticationInterceptor;

@Service
@Configuration
public class UserService implements UserDetailsService {


	@Value("${app.url.ntfc}")
	private String urlNtfc = "";

	@Value("${source.default.id}")
    private String sourceDefaultId;
	
	@Value("${ntfc.app.code}")
	private String code = "";
	
	@Value("${ntfc.app.config}")
	private String config = "";
	
    @Autowired
    private AccountLoginMapper accountLoginMapper;
    
    @Autowired
    private AccountMapper accountMapper;
    
    @Autowired
    private ControlMapper controlMapper;

	@Autowired
    private LoginMapper loginMapper;
	
	@Autowired
    private ResetMapper resetMapper;
	
	@Autowired
    private AuditService auditService;
	
	@Autowired
    private PerangkatService perangkatService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public boolean update(String userId, String kode, String password, String key, String sourceId) {
		
//		String userId = request.getParameter("username");
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
		if(userId.contains("@")) {
			param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");	
		}else {
			param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");				
		}
//		param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
		param.setClause(param.getClause() + ")");
		List<AccountLogin> lst = accountLoginMapper.getList(param);
		
		if(lst.size() > 0 && key != null) {
			
			AccountLogin account = lst.get(0);
			Reset reset = resetMapper.getEntity(key);
			
			if(reset != null && reset.isOpen() && reset.getExpired().after(new Date())) {
				
				if(reset.getAccount().equals(account.getId()) && passwordEncoder.matches("hijr_"+kode, reset.getCode())) {
					
					account.setPassword(passwordEncoder.encode(password));
					accountMapper.update(account);
					
					reset.setOpen(false);
					resetMapper.update(reset);
					
					auditService.add(AuditService.AUDIT_TYPE_RESET+"_PASSWORD_CHANGED", AuditService.AUDIT_TYPE_RESET, "Username: "+userId, account.getId(), account.getSource().getId());
					
					
					
					return true;
				}
				
				
			}
			
		}
		
		auditService.add(AuditService.AUDIT_TYPE_RESET+"_PASSWORD_FAILED", AuditService.AUDIT_TYPE_RESET, "Username: "+userId, "-", sourceId);

		return false;
	}
	
	public String reset(String userId, String sourceId) throws Exception{

//		System.out.println("POST: reset");
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
		if(userId.contains("@")) {
			param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");	
		}else {
			param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");				
		}
//		param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
		param.setClause(param.getClause() + ")");
		List<AccountLogin> lst = accountLoginMapper.getList(param);
		if(lst.size() > 0) {
			String kode = Utils.generateOtp();
			
			Reset reset = new Reset(Utils.getUUIDString());
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, 15);
			reset.setType("PASSWORD");
			reset.setAccount(lst.get(0).getId());
			reset.setCode(passwordEncoder.encode("hijr_"+kode));
			reset.setExpired(cal.getTime());
			reset.setOpen(true);
			
			resetMapper.insert(reset);
			
			Map<String, Object> payload = new HashMap<String, Object>();
			
			
			payload.put("notification_title", "Permintaan Reset Password");
			String body = "<p>Kode reset anda adalah <strong>"+kode+"</strong> dan hanya berlaku dalam waktu 15 menit.</p>";
			//body += "<p>Silahkan lakukan verifikasi pada link berikut: <br><a href=\""++"/page/verifikasi/"+id+"\">"+urlHandle+"/page/verifikasi/"+id+"</a></p>";
			
			payload.put("notification_body_text", body);

			payload.put("notification_app", code);
			payload.put("notification_settings", config);
			
			payload.put("notification_user_added", lst.get(0).getId());
			payload.put("email_recipients", "EMAIL_SINGLE_TO:"+lst.get(0).getEmail());
			
			String urlNotificationSave = urlNtfc+"/notification/save";
//			String urlNotificationSave = urlNtfc+"/send/email";
			
			Utils.postRestForm(payload, urlNotificationSave, new RestTemplate());
			

			auditService.add(AuditService.AUDIT_TYPE_RESET+"_PASSWORD_SENT", AuditService.AUDIT_TYPE_RESET, "Username: "+userId, lst.get(0).getId(), lst.get(0).getSource().getId());
			
			
			return reset.getId();
			
		}
		
		auditService.add(AuditService.AUDIT_TYPE_RESET+"_PASSWORD_FAILED", AuditService.AUDIT_TYPE_RESET, "UserID: "+userId, "-", sourceId);
		
		return null;
		
	}
	
	
    public boolean register(String email, String realname, String password, String sourceId, HttpServletRequest request) {
    	QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + AccountLogin.EMAIL + "='"+email+"'");
		List<AccountLogin> lst = accountLoginMapper.getList(param);
		if(lst.size() > 0) {
			
			
			auditService.add(AuditService.AUDIT_TYPE_REGISTER+"_FAILED", AuditService.AUDIT_TYPE_REGISTER, "Fullname: "+realname, "-", sourceId);
			
			return false;
			
		}

		Account account = new Account(Utils.getLongNumberID());
		account.setEmail(email);
		account.setFullName(realname);
		account.setPassword(passwordEncoder.encode(password));
		account.setEnabled(true);
		account.setAccountNonExpired(true);
		account.setAccountNonLocked(true);
		account.setCredentialsNonExpired(true);
		account.setUsingMfa(false);
		account.setMfaUsingWa(false);
		account.setUsingPin(false);
		account.setNotifikasiPerangkat(true);
		account.setWajibVerifikasi(true);
		
		accountMapper.insert(account);
		
		Control control = new Control(account.getId());
		control.setAdmin(false);
		control.setAccountId(account.getId());
		control.setSourceId(sourceId);
		control.setVerifikator(false);
		
		controlMapper.insert(control);
		
		perangkatService.register(request, account, sourceId);
		
		auditService.add(AuditService.AUDIT_TYPE_REGISTER+"_SUCCESS", AuditService.AUDIT_TYPE_REGISTER, "Fullname: "+account.getFullName(), account.getId(), sourceId);
		
		return true;
		
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	System.out.println(">> loadUserByUsername");
    	
    	String[] userSrc =  username.split("::");
		String userId = userSrc[0];
		String srcId = sourceDefaultId;
		if(userSrc.length > 1) {
			srcId = userSrc[1];
		}
		
		
    	
		if(!Utils.isUsernameToken(username)) {
        		
        		QueryParameter param = new QueryParameter();
        		param.setClause(param.getClause() + " AND " + Source.ID + "='"+srcId+"'");
        		param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
        		param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");
        		param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");
        		param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
        		param.setClause(param.getClause() + ")");
        		List<AccountLogin> lst = accountLoginMapper.getList(param);
        		
            if (lst.size() > 0 ) {
            		AccountLogin accountLogin = lst.get(0);
            		
            		boolean firstTime = auditService.isFirstLogin(accountLogin.getId());
            		
            		if(firstTime) {
            			auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_SUCCESS, "1st time login", accountLogin.getId(), accountLogin.getSource().getId());
            		}
            		
            		if(accountLogin.isUsingMfa() || (firstTime && accountLogin.isWajibVerifikasi())) {

            			Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
            			grantedAuthorities.add(new SimpleGrantedAuthority(TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED));
            			accountLogin.setAuthorities(grantedAuthorities);
	    	        		
            		}else {
            			
            			accountLogin.setAuthorities(collectAuthorities(accountLogin));
            			
            		}
            		
            		accountLogin.setUsername(userId);
                
                return accountLogin;
            } else {
            	 	System.out.println("UsernameNotFoundException");
                throw new UsernameNotFoundException(String.format("User with login ID [%s] not found", username));
            }
    		}
    		
//    		System.out.println(" >>> Ini 2FA login lhooo");
    	
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+srcId+"'");
    		param.setClause(param.getClause() + " AND " + Login.EXPIRE_TIME + " > NOW()");
    		param.setClause(param.getClause() + " AND " + Login.MFA_CODE_EXPIRED + " > NOW()");
    		
    		List<Login> lstLogin = loginMapper.getList(param);
    		if(lstLogin.size() > 0) {
    			Login login = lstLogin.get(0);
    			
    			param = new QueryParameter();
    			param.setClause(param.getClause() + " AND " + Source.ID + "='"+login.getSourceId()+"'");
    			param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
    			param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");
    			param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");
    			param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
    			param.setClause(param.getClause() + ")");
    			List<AccountLogin> lst = accountLoginMapper.getList(param);
    			
    		    if (lst.size() > 0 ) {
    		    		AccountLogin accountLogin = lst.get(0);
    		    		
    		    		boolean firstTime = auditService.isFirstLogin(accountLogin.getId());
                		
            		if(firstTime) {
            			auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_SUCCESS, "1st time login", accountLogin.getId(), accountLogin.getSource().getId());
            		}
            		
    		        
    		    		accountLogin.setAuthorities(collectAuthorities(accountLogin));
    		    		
    		    		accountLogin.setUsername(userId);
    		    		accountLogin.setPassword(login.getMfaCode());
    		        return accountLogin;
    		    } else {
    		    	 	
    		        throw new UsernameNotFoundException(String.format("User with login ID [%s] not found", username));
    		    }
    		} else {
	    	 	
	        throw new UsernameNotFoundException(String.format("User with login ID [%s] not using 2FA", username));
	    }
		
    }
    
    private Collection<GrantedAuthority> collectAuthorities(AccountLogin accountLogin) {
    		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		
		List<String> roles = accountLoginMapper.getListRole(accountLogin.getId());
		
	    for (String role : roles) {
	        grantedAuthorities.add(new SimpleGrantedAuthority(role.trim()));
	    }
	    
	    if(accountLogin.isAdmin()) {
	    		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
	    }
	    
	    if(accountLogin.isVerifikator()) {
	    		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_VERIFIKATOR"));
	    }
	    
	    if(!roles.contains(TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATED)) {
			grantedAuthorities.add(new SimpleGrantedAuthority(TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATED)); // ROLE_USER
	    }
	    
	    return grantedAuthorities;
    }
    
    public void sendOtp(String accessTokenValue) {
    		
		
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessTokenValue+"'");
			param.setClause(param.getClause() + " AND " + Login.EXPIRE_TIME + " > NOW()");
			
			//System.out.println("HijrTokenService.createAccessToken : " + userDetails.getId());
			
			Login login = null;
			List<Login> lstLogin = loginMapper.getList(param);
			if(lstLogin.size() > 0) {
				login = lstLogin.get(0);
			}
			
			try {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MINUTE, 10);
				
				login.setMfaCode(Utils.generateOtp());
				login.setMfaCodeExpired(cal.getTime());
			
			
				loginMapper.update(login);
				
				param = new QueryParameter();
	        		param.setClause(param.getClause() + " AND " + Source.ID + "='"+login.getSourceId()+"'");
	        		param.setClause(param.getClause() + " AND " + AccountLogin.ID + "='"+login.getAccountId()+"'");
	        		List<AccountLogin> lst = accountLoginMapper.getList(param);
	        		
	            if (lst.size() > 0 ) {
	            		AccountLogin accountLogin = lst.get(0);
	            		
	            		sendEmail(accountLogin.getEmail(), login.getMfaCode(), accountLogin.getSource().getId());
	            		
	            		auditService.addNotif(AuditService.AUDIT_CODE_NOTIF_OTP_CODE, "Email: "+accountLogin.getEmail(), accountLogin.getId(), accountLogin.getSource().getId());
	            		
	            		if(accountLogin.isMfaUsingWa()) {

	            			sendWhatsApp(accountLogin.getMobile(), login.getMfaCode());
		            		
		            		auditService.addNotif(AuditService.AUDIT_CODE_NOTIF_OTP_CODE, "WhatsApp: "+accountLogin.getMobile(), accountLogin.getId(), accountLogin.getSource().getId());
	            		}
	            		
	            }
            
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    

    public void sendEmail(String email, String kode, String sourceId) throws Exception {
//    		System.out.println(">> SENT: " + email);
    	
		Map<String, Object> payload = new HashMap<String, Object>();
//		payload.add("email", email);
//		payload.add("subject", "Kode OTP Verifikasi Login");
//		payload.add("body", "<p>Kode OTP anda adalah <strong>"+kode+"</strong> dan hanya berlaku dalam waktu 10 menit.</p><p>Ini adalah kode <strong>RAHASIA</strong>, harap jangan berikan kepada siapapun.</p>");
		
		
		payload.put("notification_title", "Kode OTP Verifikasi Login");
		payload.put("notification_body_text", "<p>Kode OTP anda adalah <strong>"+kode+"</strong> dan hanya berlaku dalam waktu 10 menit.</p><p>Ini adalah kode <strong>RAHASIA</strong>, harap jangan berikan kepada siapapun.</p>");

		payload.put("notification_app", code);
		payload.put("notification_settings", config);
		
		payload.put("notification_user_added", sourceId);
		payload.put("email_recipients", "EMAIL_SINGLE_TO:"+email);
		
		String urlNotificationSave = urlNtfc+"/notification/save";
//		String urlNotificationSave = urlNtfc+"/send/email";
		
		Utils.postRestForm(payload, urlNotificationSave, new RestTemplate());
	}
    
    public void sendWhatsApp(String mobile, String kode) throws Exception {
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("number", mobile);
		payload.put("template", "Kode OTP anda adalah "+kode);
		
		String urlNotificationSave = urlNtfc+"/send/whatsapp";
		
		Utils.postRestForm(payload, urlNotificationSave, new RestTemplate());
	}

}
