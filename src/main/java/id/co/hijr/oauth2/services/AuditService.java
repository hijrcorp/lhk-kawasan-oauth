package id.co.hijr.oauth2.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.mapper.AuditMapper;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.Audit;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.Source;

@Service
public class AuditService {
	

	@Value("${app.audit}")
	private boolean isAudit;
	
	public static final String AUDIT_TYPE_AUTH = "AUTH";
	public static final String AUDIT_TYPE_NOTIF = "NOTIF";
	public static final String AUDIT_TYPE_REGISTER = "REG";
	public static final String AUDIT_TYPE_RESET = "RESET";
	public static final String AUDIT_TYPE_DEVICE = "DEVICE";
	
	public static final String AUDIT_CODE_AUTH_LOGIN_FAILED = "LOGIN_FAILED";
	public static final String AUDIT_CODE_AUTH_LOGIN_SUCCESS = "LOGIN_SUCCESS";
	public static final String AUDIT_CODE_AUTH_LOGIN_QUERY = "LOGIN_QUERY";
	public static final String AUDIT_CODE_AUTH_LOGIN_PRE_SUCCESS = "LOGIN_PRE_SUCCESS";
	public static final String AUDIT_CODE_AUTH_LOGIN_PRE_FAILED = "LOGIN_PRE_FAILED";
	public static final String AUDIT_CODE_AUTH_LOGIN_FIRST_TIME = "LOGIN_FIRST_TIME";
	public static final String AUDIT_CODE_AUTH_LOGOUT = "LOGOUT";
	
	public static final String AUDIT_CODE_NOTIF_NEW_DEVICE = "NEW_DEVICE";
	public static final String AUDIT_CODE_NOTIF_OTP_CODE = "OTP_CODE";

	@Autowired
    private AuditMapper auditMapper;
	
	public void addNotif(String code, String dest, String accountId, String sourceId) {
		add(code, AUDIT_TYPE_NOTIF, dest, accountId, sourceId);
	}
	
	public void addAuth(String code, String info, String accountId, String sourceId) {
		add(code, AUDIT_TYPE_AUTH, info, accountId, sourceId);
	}
	
	public void add(String code, String type, String details, String accountId, String sourceId) {
		Audit audit = new Audit(Utils.getUUIDString());
		audit.setCode(code);
		audit.setType(type);
		audit.setIdSource(sourceId);
		audit.setDetails(details);
		audit.setDate(new Date());
		
		audit.setAccountAdded(accountId);
		audit.setTimestampAdded(new Date());
		
		if(isAudit) auditMapper.insert(audit);
	}
	
	public boolean isFirstLogin(String accountId) {
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Audit.ACCOUNT_ADDED + "='"+accountId+"'");
		param.setClause(param.getClause() + " AND " + Audit.CODE + "='"+AUDIT_CODE_AUTH_LOGIN_SUCCESS+"'");
		param.setLimit(1);
		
		return auditMapper.getList(param).isEmpty();
		
	}
}
