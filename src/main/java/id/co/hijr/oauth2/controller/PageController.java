package id.co.hijr.oauth2.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.WebUtils;
import id.co.hijr.oauth2.handler.CustomOAuth2RequestFactory;
import id.co.hijr.oauth2.mapper.AccountLoginMapper;
import id.co.hijr.oauth2.mapper.AccountMapper;
import id.co.hijr.oauth2.mapper.ControlMapper;
import id.co.hijr.oauth2.mapper.ResetMapper;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.Reset;
import id.co.hijr.oauth2.security.TwoFactorAuthenticationInterceptor;
import id.co.hijr.oauth2.services.AuditService;
import id.co.hijr.oauth2.services.PerangkatService;
import id.co.hijr.oauth2.services.UserService;


@Controller
public class PageController {
	
	@Value("${app.cookie.name}")
	private String cookieName;
	
	@Value("${app.cookie.used}")
	protected String cookieUsed;
	
	@Value("${source.default.id}")
	protected String sourceId;
	
	@Value("${app.url.ntfc}")
	private String urlNtfc = "";

	@Value("${ntfc.app.code}")
	private String code = "";
	
	@Value("${ntfc.app.config}")
	private String config = "";
	
	@Autowired
    private AccountLoginMapper accountLoginMapper;
	
	@Autowired
    private AuditService auditService;
	
	
	@Autowired
	private PerangkatService perangkatService;
	
	@Autowired
    private UserService userService;
	

	@RequestMapping("/login")
    public String login(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		if(request.getSession().getAttribute("username") != null) {
			return verify(model, request, response);
		}
		
		return loadIndex(model, request, response);
    }
	
	@RequestMapping("/verify")
    public String verify(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		String userId = (String)request.getSession().getAttribute("username");
		
		if (userId == null || (userId != null && request.getParameter("username")!=null && !userId.equals(request.getParameter("username")))) {
			userId = request.getParameter("username");
			
		}
		
		if(userId != null) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND (" + AccountLogin.ID + "='"+userId+"'");
			if(userId.contains("@")) {
				param.setClause(param.getClause() + " OR " + AccountLogin.EMAIL + "='"+userId+"'");	
			}else {
				param.setClause(param.getClause() + " OR " + AccountLogin.USERNAME + "='"+userId+"'");				
			}
//			param.setClause(param.getClause() + " OR " + AccountLogin.MOBILE + "='"+userId+"'");
			param.setClause(param.getClause() + ")");
			List<AccountLogin> lst = accountLoginMapper.getList(param);
			if(lst.size() > 0) {
				AccountLogin accountLogin = lst.get(0);
				
				if(accountLogin.isEnabled() && accountLogin.isAccountNonExpired() && accountLogin.isAccountNonLocked() && accountLogin.isCredentialsNonExpired()) {
					request.getSession().setAttribute("username", userId);
					
					model.addAttribute("verify", true);
					model.addAttribute("action", request.getContextPath() + "/login");
					model.addAttribute("realName", accountLogin.getFullName());
					model.addAttribute("cookieName", cookieName);
					model.addAttribute("userName", accountLogin.getId());
					model.addAttribute("email", accountLogin.getEmail());
					model.addAttribute("mobile", accountLogin.getMobile());
					if(lst.size() > 1) {
						model.addAttribute("listSrc", lst);
					}else {
						model.addAttribute("source", accountLogin.getSource().getId());
					}
					
					
					auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_QUERY+"_ACTIVE", "Username: "+userId, accountLogin.getId(), accountLogin.getSource().getId());
				}else {
					String status = "_INACTIVE";
					if(!accountLogin.isEnabled()) {
						status = "_DISABLED";
					}
					
					if(!accountLogin.isAccountNonExpired()) {
						status = "_EXPIRED";
					}
					
					if(!accountLogin.isAccountNonLocked()) {
						status = "_LOCKED";
					}
					
					if(!accountLogin.isCredentialsNonExpired()) {
						status = "_PASSWORD_EXPIRED";
					}
					
					auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_QUERY+status, "Username: "+userId, accountLogin.getId(), accountLogin.getSource().getId());
					
					request.getSession().setAttribute("username", null);
					response.sendRedirect(request.getContextPath() + "/login?error");
				}
				
				/*
				boolean verify = accountLogin.isEnabled();
				
				model.addAttribute("verify", verify);
				if(verify) {
					model.addAttribute("action", request.getContextPath() + "/login");
				}else {
					model.addAttribute("action", request.getContextPath() + "/daftar");
				}*/
				
			}else {
				request.getSession().setAttribute("username", null);
				response.sendRedirect(request.getContextPath() + "/login?error");
			}
		}else {
			request.getSession().setAttribute("username", null);
			response.sendRedirect(request.getContextPath() + "/login?error");
		}
		
		
		
		return "index";
    }
	
	
	
	
	@RequestMapping("/refresh")
    public void refresh(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
	
		Cookie cookie1 = new Cookie(cookieName, null);
		cookie1.setMaxAge(-1);
		response.addCookie(cookie1);
		
		response.sendRedirect(request.getContextPath());

    }
	
	@RequestMapping("/oauth/error")
    public String oauthError()throws Exception {
		return "error";
    }
	
	@RequestMapping("/")
    public String index(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		return loadIndex(model, request, response);
    }
	
	@RequestMapping(value="/update", method = RequestMethod.POST)
    public String updateAction(
    		@RequestParam("username") String userId,
    		@RequestParam("kode") String kode,
    		@RequestParam("password") String password,
    		@RequestParam("key") String key,
    		Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
	
		if(!userService.update(userId, kode, password, key, sourceId)) {
			response.sendRedirect(request.getContextPath() + "/reset?error");
		}else {
			request.getSession().setAttribute("username", userId);
			response.sendRedirect(request.getContextPath() + "/login?updated");
		}
		
		
		return "reset";
	}
	
	@RequestMapping(value="/reset", method = RequestMethod.GET)
    public String resetForm(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
//		System.out.println("GET: reset");
		
		String userId = (String)request.getSession().getAttribute("username");
		model.addAttribute("userId", userId);
		
		return "reset";
    }
	
	@RequestMapping(value="/reset", method = RequestMethod.POST)
    public String resetAction(
    		@RequestParam("username") String userId,
    		Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		String resetId = userService.reset(userId, sourceId);
		
		if(resetId == null) {
			response.sendRedirect(request.getContextPath() + "/reset?error");
		}else {
			response.sendRedirect(request.getContextPath() + "/reset?update&key="+resetId);
		}
		
		
		return "reset";
    }
	
	@RequestMapping(value="/daftar", method = RequestMethod.GET)
    public String daftarForm(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
//		System.out.println("GET: daftar");
		model.addAttribute("btnlabel", "Daftar");
		
		return "daftar";
    }
	
	@RequestMapping(value="/daftar", method = RequestMethod.POST)
    public String daftarAction(
    		@RequestParam("email") String email,
    		@RequestParam("realname") String realname,
    		@RequestParam("password") String password,
    		Model model, HttpServletRequest request,
			HttpServletResponse response
    		)throws Exception {
		
//		System.out.println("POST: daftar");
		model.addAttribute("btnlabel", "Daftar");
		
		if(!userService.register(email, realname, password, sourceId, request)) {

			model.addAttribute("realname", request.getParameter("realname"));
			model.addAttribute("error", "Email yang anda daftarkan sudah digunakan.");
		}else {
			
			response.sendRedirect(request.getContextPath() + "/daftar?success");
		}
		
		return "daftar";
    }
	
	protected String extractHeaderToken(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders("Authorization");
		while (headers.hasMoreElements()) { // typically there is only one (most servers enforce that)
			String value = headers.nextElement();
			if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
				String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
				// Add this here for the auth details later. Would be better to change the signature of this method.
				request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE,
						value.substring(0, OAuth2AccessToken.BEARER_TYPE.length()).trim());
				int commaIndex = authHeaderValue.indexOf(',');
				if (commaIndex > 0) {
					authHeaderValue = authHeaderValue.substring(0, commaIndex);
				}
				return authHeaderValue;
			}
		}

		return "";
	}
	
	public static String getCookieTokenValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();

		String accessTokenValue = "";
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String name = cookies[i].getName();
				String value = cookies[i].getValue();
				if (name.equals(cookieName)) {
					accessTokenValue = value;
				}
			}
		}

		return accessTokenValue;
	}
	
	
	private String loadIndex(Model model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		perangkatService.generate(request, response);
		
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			AccountLogin accountLogin = (AccountLogin)authentication.getPrincipal();
			
			String auth_state = (String)request.getSession().getAttribute(CustomOAuth2RequestFactory.SAVED_STATE_REQUEST_SESSION_ATTRIBUTE_NAME);
			
			
			String state = null;
			
			if(WebUtils.hasRole(accountLogin.getAuthorities(), TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATED)) {
				state = "FULL_AUTH";
			}else if(WebUtils.hasRole(accountLogin.getAuthorities(), TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED)) {
				state = "PRE_AUTH";
			}
			
			
			if(auth_state != null && state.equals("FULL_AUTH")) {
				request.getSession().setAttribute(CustomOAuth2RequestFactory.SAVED_STATE_REQUEST_SESSION_ATTRIBUTE_NAME, TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATED);
				return "forward:/oauth/authorize"; // Continue with the OAuth flow
			}
			
			if(cookieUsed.equals("true")) {
				String accessTokenValue = WebUtils.getCookieTokenValue(request, cookieName);
				
				try {
				    DecodedJWT jwt = JWT.decode(accessTokenValue);
					model.addAttribute("token", accessTokenValue);
//					model.addAttribute("verify", true);
				    model.addAttribute("action", request.getContextPath() + "/login");
				    
				    
				    model.addAttribute("cookieName", cookieName);
				    model.addAttribute("userName", jwt.getClaim("account_id").asString());
					model.addAttribute("realName",jwt.getClaim("full_name").asString());
					model.addAttribute("organization", jwt.getClaim("organization_id").asString());
					model.addAttribute("picture", jwt.getClaim("account_avatar").asString());
					
					
				} catch (JWTDecodeException e){
				    //Invalid token
					e.printStackTrace();
				}
				
				/*
				if(state != null && cookie_state !=null) {
					if(!cookie_state.equals(state)) {
						return "forward:/oauth/authorize"; // Continue with the OAuth flow
					}
				}
				*/
			}else {
				System.out.println("lewat login");
				
				model.addAttribute("cookieName", cookieName);
				model.addAttribute("realName", accountLogin.getFullName());
				model.addAttribute("userName", accountLogin.getId());
				model.addAttribute("email", accountLogin.getEmail());
				model.addAttribute("mobile", accountLogin.getMobile());
				model.addAttribute("action", request.getContextPath() + "/login");
			}
			
			if(state != null) model.addAttribute("state",state);
			
			//System.out.println("udah login");
			
		} catch (Exception exception){
		    //Invalid token
			//exception.printStackTrace();
			
			System.out.println(exception.getMessage());
			
			if(cookieUsed.equals("true")) {
				String accessTokenValue = WebUtils.getCookieTokenValue(request, cookieName);

				if (!accessTokenValue.equals("")) {
					
					try {
					    DecodedJWT jwt = JWT.decode(accessTokenValue);
					    
					    
					    model.addAttribute("verify", true);
					    model.addAttribute("action", request.getContextPath() + "/login");
					    
					    
					    model.addAttribute("cookieName", cookieName);
					    model.addAttribute("userName", jwt.getClaim("account_id").asString());
						model.addAttribute("realName",jwt.getClaim("full_name").asString());
						model.addAttribute("organization", jwt.getClaim("organization_id").asString());
						model.addAttribute("picture", jwt.getClaim("account_avatar").asString());
						
						
						
						model.addAttribute("token", accessTokenValue);
						
						if(jwt.getClaim("authorities") != null ) {
							if(WebUtils.hasRole(jwt.getClaim("authorities").asList(String.class), TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATED)) {
								model.addAttribute("state", "FULL_AUTH");
							}else if(WebUtils.hasRole(jwt.getClaim("authorities").asList(String.class), TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED)) {
								model.addAttribute("state", "PRE_AUTH");
							}
						}

						
					} catch (JWTDecodeException e){
					    //Invalid token
						e.printStackTrace();
					}
				}
			}
		}

		return "index";
	}
	

	@RequestMapping("/resend")
    public String resend(Model model, HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		if(cookieUsed.equals("true")) {
			String accessTokenValue = WebUtils.getCookieTokenValue(request, cookieName);

			
			DecodedJWT jwt = JWT.decode(accessTokenValue);
		    
			if(jwt.getClaim("authorities") != null && WebUtils.hasRole(jwt.getClaim("authorities").asList(String.class), TwoFactorAuthenticationInterceptor.ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED)) {
				model.addAttribute("resend", true);
				userService.sendOtp(accessTokenValue);	
			}
		}
		
		
		return loadIndex(model, request, response);
    }
	
	
}
