package id.co.hijr.oauth2.controller;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.oauth2.mapper.LoginMapper;
import id.co.hijr.oauth2.mapper.SQLMapper;
import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.services.AuditService;
import id.co.hijr.oauth2.services.PerangkatService;
import id.co.hijr.oauth2.services.UserService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ServiceController {
	
	@Value("${source.default.id}")
	protected String sourceId;

	@Autowired
    private LoginMapper loginMapper;
	
	@Resource(name="serverTokenServices")
	ConsumerTokenServices tokenServices;
	
	@Autowired
    private AuditService auditService;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Autowired
	private PerangkatService perangkatService;
	
	@Autowired
    private UserService userService;
	
	@Autowired
    private SQLMapper sqlMapper;
	
	private String extractHeaderToken(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders("Authorization");
		while (headers.hasMoreElements()) { // typically there is only one (most servers enforce that)
			String value = headers.nextElement();
			if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
				String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
				// Add this here for the auth details later. Would be better to change the signature of this method.
				request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE,
						value.substring(0, OAuth2AccessToken.BEARER_TYPE.length()).trim());
				int commaIndex = authHeaderValue.indexOf(',');
				if (commaIndex > 0) {
					authHeaderValue = authHeaderValue.substring(0, commaIndex);
				}
				return authHeaderValue;
			}
		}

		return "";
	}
	
	private String extractHeaderCredentials(HttpServletRequest request) {
		final String authorization = request.getHeader("Authorization");
		if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
		    // Authorization: Basic base64credentials
		    String base64Credentials = authorization.substring("Basic".length()).trim();
		    byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
		    String credentials = new String(credDecoded, StandardCharsets.UTF_8);
		    // credentials = username:password
		    //final String[] values = credentials.split(":", 2);
		    return credentials;
		}
		return "";
	}
	
	public boolean isValidBasicCredentials(String credentials) {
		// TODO Auto-generated method stub
		
		final String[] values = credentials.split(":", 2);
		
		Map<String, Object> map = sqlMapper.selectOne("SELECT * FROM oauth_client_details WHERE client_id='"+values[0]+"'");
		
		if(map != null && !map.isEmpty()) {
			String secret = map.get("client_secret").toString();
			// TODO: hilangin validasi tanpa encoding
			if(passwordEncoder.matches(values[1], secret) || values[1].equals(secret)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	public boolean isValidAccessToken(String accessToken) {
		// TODO Auto-generated method stub
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessToken+"'");
		param.setClause(param.getClause() + " AND " + Login.EXPIRE_TIME + " > NOW()");
		
		List<Login> lstLogin = loginMapper.getList(param);
		if(lstLogin.size() > 0) {
			return true;
		}
		
		return false;
		
	}
	
	@RequestMapping(value="/otp/send", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> sendOtp(HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		String accessTokenValue = extractHeaderToken(request);
		
		
		if(!isValidAccessToken(accessTokenValue)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Unauthorized\", \"message\": \"Unauthorized user\" }");
		}
		
		userService.sendOtp(accessTokenValue);
		
		return ResponseEntity.ok().body("{ \"timestamp\": "+new Date().getTime()+",\"message\": \"Kode OTP telah dikirim.\" }");
    }
	
	@RequestMapping(value="/device/register", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> registerDevice(
			HttpServletRequest request,
			@RequestParam("devid") String devid,
			@RequestParam("tipe") String tipe,
			@RequestParam("nama") String nama
    		) throws Exception {
		
		String accessTokenValue = extractHeaderToken(request);
		
		
		if(!isValidAccessToken(accessTokenValue)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Unauthorized\", \"message\": \"Unauthorized user\" }");
		}
		
		perangkatService.register(request, accessTokenValue, devid, tipe, nama);
		
        return ResponseEntity.ok().body("{ \"timestamp\": "+new Date().getTime()+",\"message\": \"Registrasi perangkat telah berhasil\" }");
    }
	
	@RequestMapping(value="/oauth/revoke", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> removeToken(
			HttpServletRequest request
    		) throws Exception {
		
		String accessTokenValue = extractHeaderToken(request);
		
		
		if(!isValidAccessToken(accessTokenValue)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Unauthorized\", \"message\": \"Unauthorized user\" }");
		}
		
		tokenServices.revokeToken(accessTokenValue);
	
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Login.ACCESS_TOKEN + "='"+accessTokenValue+"'");
		
		List<Login> lstLogin = loginMapper.getList(param);
		
        for(Login login: lstLogin) {
        		login.setStatus(Login.LOGOUT);
	        if(login.getExpireTime().before(new Date())) {
	        		login.setStatus(Login.EXPIRED);
            }
	        loginMapper.insertArchive(login); // 1
    			loginMapper.delete(login); // 2
    			
    			auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGOUT, "IP Public: "+request.getRemoteAddr() ,login.getAccountId(), login.getSourceId());
        }
		
        return ResponseEntity.ok().body("{ \"timestamp\": "+new Date().getTime()+",\"message\": \"Access token berhasil di revoke\" }");
    }
	
	@RequestMapping(value="/account/register", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> registerAccount(
    		@RequestParam("email") String email,
    		@RequestParam("realname") String realname,
    		@RequestParam("password") String password,
			HttpServletRequest request
    		) throws Exception {
		
		String credentials = extractHeaderCredentials(request);
		
		if(!isValidBasicCredentials(credentials)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Unauthorized\", \"message\": \"Unauthorized user\" }");
		}
		
		if(!userService.register(email, realname, password, sourceId, request)) {

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Pendaftaran Gagal\", \"message\": \"Email yang anda daftarkan sudah digunakan\" }");
		}
		
        return ResponseEntity.ok().body("{ \"timestamp\": "+new Date().getTime()+",\"message\": \"Pendaftaran akun pengguna telah berhasil\" }");
    }
	
	@RequestMapping(value="/password/reset", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> resetPassword(
			HttpServletRequest request,
			@RequestParam("username") String userId
    		) throws Exception {
		
		String credentials = extractHeaderCredentials(request);
		
		if(!isValidBasicCredentials(credentials)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Unauthorized\", \"message\": \"Unauthorized user\" }");
		}
		
		String resetId = userService.reset(userId, sourceId);
		
		if(resetId == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Pengiriman Gagal\", \"message\": \"Username atau email anda belum terdaftar\" }");
		}
		
        return ResponseEntity.ok().body("{ \"timestamp\": "+new Date().getTime()+",\"key\": \""+resetId+"\", \"message\": \"Kode reset password telah dikirim ke email anda\" }");
    }
	
	@RequestMapping(value="/password/update", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> updatePassword(
    		@RequestParam("username") String userId,
    		@RequestParam("kode") String kode,
    		@RequestParam("password") String password,
    		@RequestParam("key") String key,
			HttpServletRequest request
    		) throws Exception {
		
		String credentials = extractHeaderCredentials(request);
		
		if(!isValidBasicCredentials(credentials)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Unauthorized\", \"message\": \"Unauthorized user\" }");
		}
		
		if(!userService.update(userId, kode, password, key, sourceId)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{ \"timestamp\": "+new Date().getTime()+",\"error\": \"Perubahan Gagal\", \"message\": \"Perubahan password gagal tersimpan\" }");
		}
		
		
        return ResponseEntity.ok().body("{ \"timestamp\": "+new Date().getTime()+",\"message\": \"Password akun anda telah berhasil diubah\" }");
    }
	
}
