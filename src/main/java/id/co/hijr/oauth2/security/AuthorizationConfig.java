package id.co.hijr.oauth2.security;

import java.util.Arrays;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import id.co.hijr.oauth2.handler.CustomOAuth2RequestFactory;
import id.co.hijr.oauth2.services.HijrTokenService;

@Configuration
@EnableAuthorizationServer
public class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {


	@Value("${security.oauth2.token.key}")
	private String tokenKey;
	
	@Value("${security.oauth2.keystore.file}")
    private String keystoreFile;
    
    @Value("${security.oauth2.keystore.pass}")
    private String keystorePass;
    
    @Value("${security.oauth2.keystore.alias}")
    private String keystoreAlias;


	@Autowired
	public UserDetailsService userDetailsService;
	
	@Autowired
	public ClientDetailsService clientDetailsService;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
        .addInterceptor(twoFactorAuthenticationInterceptor()).requestFactory(customOAuth2RequestFactory())
        .authorizationCodeServices(new JdbcAuthorizationCodeServices(dataSource))
                .authenticationManager(this.authenticationManager)
                .userDetailsService(userDetailsService)
                .tokenServices(serverTokenServices())
                ;
    }
	
	@Bean
    public OAuth2RequestFactory customOAuth2RequestFactory() {
        return new CustomOAuth2RequestFactory(clientDetailsService);
    }
	
	@Bean
    public HandlerInterceptor twoFactorAuthenticationInterceptor() {
        return new TwoFactorAuthenticationInterceptor();
    }

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {

		oauthServer
				.allowFormAuthenticationForClients().tokenKeyAccess("permitAll()")//.checkTokenAccess("isAuthenticated()")
		 .checkTokenAccess("permitAll()")

		;
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients
				.jdbc(dataSource)
		;
	}
	
	@Bean
    public JwtAccessTokenConverter serverAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory =
                new KeyStoreKeyFactory(
                        new ClassPathResource(this.keystoreFile),
                        this.keystorePass.toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair(this.keystoreAlias));
        return converter;
        
    }
	
	/*
	@Bean
	public JwtAccessTokenConverter serverAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(tokenKey);
		return converter;

	}
	*/

	@Bean
	public TokenStore serverTokenStore() {
		return new JwtTokenStore(serverAccessTokenConverter());

	}

	@Bean
	@Primary
	public DefaultTokenServices serverTokenServices() {
		HijrTokenService tokenService = new HijrTokenService();
		tokenService.setClientDetailsService(new JdbcClientDetailsService(dataSource));
		tokenService.setTokenStore(serverTokenStore());
		tokenService.setTokenEnhancer(serverTokenEnhancerChain());
		tokenService.setSupportRefreshToken(true);
		tokenService.setReuseRefreshToken(true);
		return tokenService;
	}

	@Bean
	public TokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}

	@Bean
	public TokenEnhancerChain serverTokenEnhancerChain() {
		final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), serverAccessTokenConverter()));
		return tokenEnhancerChain;
	}

	@Bean
	public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore) {
		TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
		handler.setTokenStore(tokenStore);
		handler.setRequestFactory(new DefaultOAuth2RequestFactory(new JdbcClientDetailsService(dataSource)));
		handler.setClientDetailsService(new JdbcClientDetailsService(dataSource));
		return handler;
	}

	@Bean
	public ApprovalStore approvalStore(TokenStore tokenStore) throws Exception {
		TokenApprovalStore store = new TokenApprovalStore();
		store.setTokenStore(tokenStore);
		return store;
	}

}
