package id.co.hijr.oauth2.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import id.co.hijr.oauth2.handler.AuthenticationSuccessHandler;
import id.co.hijr.oauth2.handler.CustomLogoutHandler;
import id.co.hijr.oauth2.handler.CustomLogoutSuccessHandler;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public UserDetailsService userDetailsService;

	@Autowired
	private UserAuthenticatoinProvider accountAuthenticationProvider;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private DataSource dataSource;
	
	@Value("${security.oauth2.token.key}")
	private String tokenKey;
	
	public TokenStore serverTokenStore() {
		return new JwtTokenStore(serverAccessTokenConverter());

	}

	public JwtAccessTokenConverter serverAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(tokenKey);
		return converter;

	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable()
		.authorizeRequests()
        .antMatchers("/resources/**","/", "/api/**", "/login**","/password**","/reset**","/refresh**","/update**","/daftar**","/verify**").permitAll() 
        .anyRequest().authenticated()
        .and()
        .formLogin().loginPage("/login").permitAll().defaultSuccessUrl("/login?success")
        .successHandler(authenticationSuccessHandler())
        .and()
        .rememberMe().rememberMeParameter("remember-me").tokenRepository(tokenRepository())
        .and()
        .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        .addLogoutHandler(customLogoutHandler())
        .logoutSuccessHandler(new CustomLogoutSuccessHandler())
        .invalidateHttpSession(true);
	}
	
	@Bean
	public AuthenticationSuccessHandler authenticationSuccessHandler() {
	    return new AuthenticationSuccessHandler();
	}
	
	@Bean
	public CustomLogoutHandler customLogoutHandler() {
	    return new CustomLogoutHandler();
	}
	
	@Bean
	  public PersistentTokenRepository tokenRepository() {
	    JdbcTokenRepositoryImpl jdbcTokenRepositoryImpl=new JdbcTokenRepositoryImpl();
	    jdbcTokenRepositoryImpl.setDataSource(dataSource);
	    return jdbcTokenRepositoryImpl;
	  }
	

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(accountAuthenticationProvider).userDetailsService(userDetailsService)
				.passwordEncoder(passwordEncoder);
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		// provides the default AuthenticationManager as a Bean
		return super.authenticationManagerBean();
	}
}
