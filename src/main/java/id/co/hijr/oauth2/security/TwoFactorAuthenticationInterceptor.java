package id.co.hijr.oauth2.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import id.co.hijr.oauth2.AuthenticationUtil;
import id.co.hijr.oauth2.handler.CustomOAuth2RequestFactory;

public class TwoFactorAuthenticationInterceptor implements HandlerInterceptor {
	
	public static final String ROLE_TWO_FACTOR_AUTHENTICATED = "ROLE_USER";
	public static final String ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED = "ROLE_PRE_AUTH";
	
	public static final String TWO_FACTOR_AUTHENTICATION_PATH = "/";
	
	private OAuth2RequestFactory oAuth2RequestFactory;

    @Autowired
    public void setClientDetailsService(ClientDetailsService clientDetailsService) {
        oAuth2RequestFactory = new DefaultOAuth2RequestFactory(clientDetailsService);
    }

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}
	
	private Map<String, String> paramsFromRequest(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>();
        for (Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            params.put(entry.getKey(), entry.getValue()[0]);
        }
        return params;
    }
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		
//		System.out.println(">>>>  preHandle");
		
		// Check if the user hasn't done the two factor authentication.
        if (AuthenticationUtil.isAuthenticated()) {
        	
        		if(AuthenticationUtil.hasAuthority(ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED)) {
        			request.getSession().setAttribute(CustomOAuth2RequestFactory.SAVED_STATE_REQUEST_SESSION_ATTRIBUTE_NAME, ROLE_TWO_FACTOR_AUTHENTICATION_ENABLED);
                	
//            		System.out.println("mampiiir....");
           
                redirectStrategy.sendRedirect(request, response,
                        ServletUriComponentsBuilder.fromCurrentContextPath()
                            .path(TWO_FACTOR_AUTHENTICATION_PATH)
                            .toUriString());
           	
                return false;
        		}
        		     
        }else {
//        		System.out.println(" kalo disiniiii... ");
        	
        	
        		AuthorizationRequest authorizationRequestSession = (AuthorizationRequest) request.getSession().getAttribute(CustomOAuth2RequestFactory.SAVED_AUTHORIZATION_REQUEST_SESSION_ATTRIBUTE_NAME);
        		
        		if(authorizationRequestSession == null) {
        			AuthorizationRequest authorizationRequest = oAuth2RequestFactory.createAuthorizationRequest(paramsFromRequest(request));
    	            
	    	        	request.getSession().setAttribute(CustomOAuth2RequestFactory.SAVED_AUTHORIZATION_REQUEST_SESSION_ATTRIBUTE_NAME, authorizationRequest);
	    	        	
//	    	        	System.out.println("massuskkk...." + authorizationRequest);	
        		}
        	
	        	
        }

		
		return true;
	}
	

}
