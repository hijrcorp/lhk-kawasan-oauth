package id.co.hijr.oauth2.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import id.co.hijr.oauth2.Utils;
import id.co.hijr.oauth2.mapper.LoginMapper;
import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.services.AuditService;

@Component
public class UserAuthenticatoinProvider extends AbstractUserDetailsAuthenticationProvider{
	
	/**
     * A Spring Security UserDetailsService implementation based upon the
     * Account entity model.
     */
    @Autowired
    private UserDetailsService userDetailsService;

	
    /**
     * A PasswordEncoder instance to hash clear test password values.
     */
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private LoginMapper loginMapper;

	@Autowired
    private AuditService auditService;
    
    @Value("${security.oauth2.client.id}")
    private String clientId;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken token) throws AuthenticationException {

    		AccountLogin accountLogin = (AccountLogin)userDetails;
    	
        if (token.getCredentials() == null || userDetails.getPassword() == null) {
            throw new BadCredentialsException("Credentials may not be null.");
        }
        
        if(Utils.isMfaCode(userDetails.getPassword())) {
        	
        	/*
            if (!userDetails.getPassword().equals(token.getCredentials())) {
            		auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_FAILED, "2FA login", accountLogin.getId(), accountLogin.getSource().getId());
            	
                throw new BadCredentialsException("Invalid OTP code.");
            }*/
            
            QueryParameter param = new QueryParameter();
	    		param.setClause(param.getClause() + " AND " + Login.USERNAME + "='"+userDetails.getUsername()+"'");
	    		param.setClause(param.getClause() + " AND " + Login.MFA_CODE + "='"+userDetails.getPassword()+"'");
	    		param.setClause(param.getClause() + " AND " + Login.MFA_CODE_EXPIRED + " > NOW()");
	    		
	    		List<Login> lstLogin = loginMapper.getList(param);
	    		if(lstLogin.size() > 0) {
	    			Login login = lstLogin.get(0);
	    			if(login.getClientId().equals(clientId) && accountLogin.isUsingMfa()) {
	    				login.setMfaCode(null);
	    				login.setMfaCodeExpired(null);
	    				loginMapper.update(login);
	    			}else {
	    				loginMapper.delete(login);	
	    			}
	    			

		    		auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_SUCCESS, "2FA login", accountLogin.getId(), accountLogin.getSource().getId());

	    		}else {
	    			
	    			auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_FAILED, "2FA login", accountLogin.getId(), accountLogin.getSource().getId());
	            	
	            throw new BadCredentialsException("Invalid OTP code.");
	    		}
	    		
        }else {

            if (!passwordEncoder.matches((String) token.getCredentials(), userDetails.getPassword())) {
            		auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_FAILED, "password login", accountLogin.getId(), accountLogin.getSource().getId());
            	
                throw new BadCredentialsException("Invalid credentials.");
            }
            
            auditService.addAuth(AuditService.AUDIT_CODE_AUTH_LOGIN_SUCCESS, "password login", accountLogin.getId(), accountLogin.getSource().getId());

        }
        
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken token) throws AuthenticationException {
    		return userDetailsService.loadUserByUsername(username);
    }

}




























