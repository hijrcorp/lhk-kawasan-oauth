package id.co.hijr.oauth2.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import id.co.hijr.oauth2.model.AccountLogin;

@Configuration
public class CustomTokenEnhancer implements TokenEnhancer {
	
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
	 	final Map<String, Object> additionalInfo = new HashMap<>();

	 	AccountLogin user = (AccountLogin) authentication.getPrincipal();
	 	
	 	additionalInfo.put("source_id", user.getSource().getId());
        additionalInfo.put("source_name", user.getSource().getName());
        additionalInfo.put("account_id", user.getId());
        additionalInfo.put("full_name", user.getFullName());
        
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        
        return accessToken;
    }

}
