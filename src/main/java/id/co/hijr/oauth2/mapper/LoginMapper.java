package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.Login;
import id.co.hijr.oauth2.model.QueryParameter;



@Mapper
public interface LoginMapper {
	
	@Insert("INSERT INTO hijr_login (id_login, access_token_login, client_id_login, username_login, refresh_token_login, created_time_login, expire_time_login, token_object_login, account_id_login, source_id_login, mfa_code_login, mfa_code_expired_login) VALUES (#{id:VARCHAR}, #{accessToken}, #{clientId:VARCHAR}, #{username:VARCHAR}, #{refreshToken}, #{createdTime:TIMESTAMP}, #{expireTime:TIMESTAMP}, #{tokenObject:BLOB}, #{accountId:VARCHAR}, #{sourceId:VARCHAR}, #{mfaCode:VARCHAR}, #{mfaCodeExpired:TIMESTAMP})")
	void insert(Login login);

	@Update("UPDATE hijr_login SET id_login=#{id:VARCHAR}, access_token_login=#{accessToken}, client_id_login=#{clientId:VARCHAR}, username_login=#{username:VARCHAR}, refresh_token_login=#{refreshToken}, created_time_login=#{createdTime:TIMESTAMP}, expire_time_login=#{expireTime:TIMESTAMP}, token_object_login=#{tokenObject:BLOB}, account_id_login=#{accountId:VARCHAR}, source_id_login=#{sourceId:VARCHAR}, mfa_code_login=#{mfaCode:VARCHAR}, mfa_code_expired_login=#{mfaCodeExpired:TIMESTAMP} WHERE id_login=#{id}")
	void update(Login login);

	@Delete("DELETE FROM hijr_login WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_login WHERE id_login=#{id}")
	void delete(Login login);

	List<Login> getList(QueryParameter param);

	Login getEntity(String id);

	long getCount(QueryParameter param);
	

	/**********************************************************************/
	

	
	@Insert("INSERT INTO hijr_login_archive (id_login, access_token_login, client_id_login, username_login, refresh_token_login, created_time_login, expire_time_login, token_object_login, account_id_login, source_id_login, status_login) VALUES (#{id:VARCHAR}, #{accessToken}, #{clientId:VARCHAR}, #{username:VARCHAR}, #{refreshToken}, #{createdTime:DATE}, #{expireTime:DATE}, #{tokenObject:BLOB}, #{accountId:VARCHAR}, #{sourceId:VARCHAR}, #{status:VARCHAR})")
	void insertArchive(Login login);
	
	
}
