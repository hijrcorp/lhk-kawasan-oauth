package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.Audit;
import id.co.hijr.oauth2.model.QueryParameter;



@Mapper
public interface AuditMapper {
	
	@Insert("INSERT INTO hijr_audit (id_audit, date_audit, type_audit, code_audit, details_audit, id_source_audit, account_added_audit, timestamp_added_audit) VALUES (#{id:VARCHAR}, #{date:DATE}, #{type:VARCHAR}, #{code:VARCHAR}, #{details:VARCHAR}, #{idSource:VARCHAR}, #{accountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP})")
	void insert(Audit audit);

	@Delete("DELETE FROM hijr_audit WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_audit WHERE id_audit=#{id}")
	void delete(Audit audit);

	List<Audit> getList(QueryParameter param);

	Audit getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
