package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.Account;
import id.co.hijr.oauth2.model.QueryParameter;



@Mapper
public interface AccountMapper {
	
	@Insert("INSERT INTO hijr_account (id_account, username_account, password_account, email_account, full_name_account, mobile_account, enabled_account, account_non_expired_account, credentials_non_expired_account, account_non_locked_account, using_mfa_account, mfa_using_wa_account, using_pin_account, notifikasi_perangkat_account, wajib_verifikasi_account) VALUES (#{id:VARCHAR}, #{username:VARCHAR}, #{password:VARCHAR}, #{email:VARCHAR}, #{fullName:VARCHAR}, #{mobile:VARCHAR}, #{enabled:BOOLEAN}, #{accountNonExpired:BOOLEAN}, #{credentialsNonExpired:BOOLEAN}, #{accountNonLocked:BOOLEAN}, #{usingMfa:BOOLEAN}, #{mfaUsingWa:BOOLEAN}, #{usingPin:BOOLEAN}, #{notifikasiPerangkat:BOOLEAN}, #{wajibVerifikasi:BOOLEAN})")
	void insert(Account account);

	@Update("UPDATE hijr_account SET id_account=#{id:VARCHAR}, username_account=#{username:VARCHAR}, password_account=#{password:VARCHAR}, email_account=#{email:VARCHAR}, full_name_account=#{fullName:VARCHAR}, mobile_account=#{mobile:VARCHAR}, enabled_account=#{enabled:BOOLEAN}, account_non_expired_account=#{accountNonExpired:BOOLEAN}, credentials_non_expired_account=#{credentialsNonExpired:BOOLEAN}, account_non_locked_account=#{accountNonLocked:BOOLEAN}, using_mfa_account=#{usingMfa:BOOLEAN}, mfa_using_wa_account=#{mfaUsingWa:BOOLEAN}, using_pin_account=#{usingPin:BOOLEAN}, notifikasi_perangkat_account=#{notifikasiPerangkat:BOOLEAN}, wajib_verifikasi_account=#{wajibVerifikasi:BOOLEAN} WHERE id_account=#{id}")
	void update(Account account);

	@Delete("DELETE FROM hijr_account WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_account WHERE id_account=#{id}")
	void delete(Account account);

	List<Account> getList(QueryParameter param);

	Account getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
