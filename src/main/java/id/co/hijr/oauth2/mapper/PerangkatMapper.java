package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.Perangkat;
import id.co.hijr.oauth2.model.QueryParameter;

@Mapper
public interface PerangkatMapper {

	@Insert("INSERT INTO hijr_perangkat (id_perangkat, kode_perangkat, nama_perangkat, tipe_perangkat, token_perangkat, jenis_token_perangkat, is_terverifikasi_perangkat, id_source_perangkat, id_account_perangkat, account_added_perangkat, timestamp_added_perangkat, account_modified_perangkat, timestamp_modified_perangkat) VALUES (#{id:VARCHAR}, #{kode:VARCHAR}, #{nama:VARCHAR}, #{tipe:VARCHAR}, #{token:VARCHAR}, #{jenisToken:VARCHAR}, #{terverifikasi:BOOLEAN}, #{idSource:VARCHAR}, #{idAccount:VARCHAR}, #{accountAdded:VARCHAR}, #{timestampAdded:TIMESTAMP}, #{accountModified:VARCHAR}, #{timestampModified:TIMESTAMP})")
	void insert(Perangkat perangkat);

	@Update("UPDATE hijr_perangkat SET id_perangkat=#{id:VARCHAR}, kode_perangkat=#{kode:VARCHAR}, nama_perangkat=#{nama:VARCHAR}, tipe_perangkat=#{tipe:VARCHAR}, token_perangkat=#{token:VARCHAR}, jenis_token_perangkat=#{jenisToken:VARCHAR}, is_terverifikasi_perangkat=#{terverifikasi:BOOLEAN}, id_source_perangkat=#{idSource:VARCHAR}, id_account_perangkat=#{idAccount:VARCHAR}, account_added_perangkat=#{accountAdded:VARCHAR}, timestamp_added_perangkat=#{timestampAdded:TIMESTAMP}, account_modified_perangkat=#{accountModified:VARCHAR}, timestamp_modified_perangkat=#{timestampModified:TIMESTAMP} WHERE id_perangkat=#{id}")
	void update(Perangkat perangkat);

	@Delete("DELETE FROM hijr_perangkat WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_perangkat WHERE id_perangkat=#{id}")
	void delete(Perangkat perangkat);

	List<Perangkat> getList(QueryParameter param);

	Perangkat getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
