package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.oauth2.model.QueryParameter;
import id.co.hijr.oauth2.model.Reset;



@Mapper
public interface ResetMapper {
	
	@Insert("INSERT INTO hijr_reset (id_reset, type_reset, expired_reset, account_reset, code_reset, is_open_reset) VALUES (#{id:VARCHAR}, #{type:VARCHAR}, #{expired:TIMESTAMP}, #{account:VARCHAR}, #{code:VARCHAR}, #{open:BOOLEAN})")
	void insert(Reset reset);

	@Update("UPDATE hijr_reset SET id_reset=#{id:VARCHAR}, type_reset=#{type:VARCHAR}, expired_reset=#{expired:TIMESTAMP}, account_reset=#{account:VARCHAR}, code_reset=#{code:VARCHAR}, is_open_reset=#{open:BOOLEAN} WHERE id_reset=#{id}")
	void update(Reset reset);

	@Delete("DELETE FROM hijr_reset WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_reset WHERE id_reset=#{id}")
	void delete(Reset reset);

	List<Reset> getList(QueryParameter param);

	Reset getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
