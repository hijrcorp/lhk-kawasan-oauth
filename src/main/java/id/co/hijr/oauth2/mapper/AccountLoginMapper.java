package id.co.hijr.oauth2.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import id.co.hijr.oauth2.model.AccountLogin;
import id.co.hijr.oauth2.model.QueryParameter;



@Mapper
public interface AccountLoginMapper {

	List<AccountLogin> getList(QueryParameter param);

	AccountLogin getEntity(String id);

	long getCount(QueryParameter param);
	

	/**********************************************************************/
	
	List<String> getListRole(String param);
}
