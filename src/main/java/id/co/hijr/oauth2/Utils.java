package id.co.hijr.oauth2;

import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.KeyGenerator;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

public class Utils {

	private static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;
	
	public static String getUUIDString() {
		NoArgGenerator timeBasedGenerator = Generators.timeBasedGenerator();

        //Generate time based UUID
        UUID firstUUID = timeBasedGenerator.generate();
		return firstUUID.toString();
	}
	
	public static Date getUUIDDate(String uuidString) {
		return new Date((UUID.fromString(uuidString).timestamp() - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000);
	}
	
	public static long getDateDiffFromNow(Date d1) {
		Date d2 = new Date();
        long seconds = (d2.getTime()-d1.getTime())/1000;
        return seconds;
	}
	
	public static boolean ip(String text) {
	    Pattern p = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
	    Matcher m = p.matcher(text);
	    return m.find();
	}
	

	public static String getLongNumberID(int idx, int digit) {
		return (new Date().getTime() + String.format("%0"+digit+"d", idx));
	}
	
	public static int randomPlus() {
		int min = 100;
		int max = 999;

		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}
	
	public static String getLongNumberID() {
		return (new Date().getTime() + "" + randomPlus());
	}

    public static boolean isUsernameToken(String username) {
    		String[] userSrc =  username.split("::");
    		if(userSrc.length > 1 && userSrc[1].length() > 200) {
    			return true;
    		}
    		
    		return false;
    }
    
    public static boolean isMfaCode(String password) {
		if(password.length() < 8 && password.matches("[0-9]+")) {
			return true;
		}
		
		return false;
    }
    

    public static String generateOtp() throws Exception {
		final TimeBasedOneTimePasswordGenerator totp = new TimeBasedOneTimePasswordGenerator();
		
		final Key key;
		{
		    final KeyGenerator keyGenerator = KeyGenerator.getInstance(totp.getAlgorithm());

		    // SHA-1 and SHA-256 prefer 64-byte (512-bit) keys; SHA512 prefers 128-byte (1024-bit) keys
		    keyGenerator.init(512);

		    key = keyGenerator.generateKey();
		}
		
		final Instant now = Instant.now();
		
		int code = totp.generateOneTimePassword(key, now);
//		int code = 12345678;
		String strcode = code+"";
		int left = strcode.length() - 6;
		
		if(left > 0) {
			strcode = strcode.substring(0, 6);
		}else {
			strcode = String.format("%0"+6+"d", code);
		}
		
		return strcode;
	}
    
    public static Map<String,Object> postRestJson(Map<String, Object> payload, String url, RestTemplate restTemplate) {
		HttpHeaders headers = new HttpHeaders();
		
		headers.setContentType(MediaType.APPLICATION_JSON);
		
        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(payload, headers);
        
        ResponseEntity<String> loginResponse = restTemplate.postForEntity(url, request, String.class);
//        System.out.println("hasil api: " + loginResponse);
        Map<String,Object> map = new HashMap<String,Object>();
        ObjectMapper mapper = new ObjectMapper();

        try {
            //convert JSON string to Map
           map = mapper.readValue(loginResponse.getBody(), new TypeReference<HashMap<String,Object>>(){});
        } catch (Exception e) {
             e.printStackTrace();
        }
        
        
        
        return map;
	}
    
    
    
    public static Map<String,Object> postRestForm(Map<String, Object> payload, String url, RestTemplate restTemplate) {
		HttpHeaders headers = new HttpHeaders();
		
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		
		LinkedMultiValueMap<String, Object> data = new LinkedMultiValueMap<String, Object>();
		
		for (Map.Entry<String,Object> entry : payload.entrySet()) {
			data.add(entry.getKey(), entry.getValue());
		}
		
		
        HttpEntity<LinkedMultiValueMap<String, Object>> request = new HttpEntity<LinkedMultiValueMap<String, Object>>(data, headers);
        
        ResponseEntity<String> loginResponse = restTemplate.postForEntity(url, request, String.class);
        System.out.println("hasil api: " + loginResponse);
        Map<String,Object> map = new HashMap<String,Object>();
        ObjectMapper mapper = new ObjectMapper();

        try {
            //convert JSON string to Map
           map = mapper.readValue(loginResponse.getBody(), new TypeReference<HashMap<String,Object>>(){});
        } catch (Exception e) {
             e.printStackTrace();
        }
        
        
        
        return map;
	}
    
    public static Map<String,Object> getRest(String url, RestTemplate restTemplate) {
		

        ResponseEntity<String> loginResponse = restTemplate.getForEntity(url, String.class);
//        System.out.println("hasil api: " + loginResponse);
        Map<String,Object> map = new HashMap<String,Object>();
        ObjectMapper mapper = new ObjectMapper();

        try {
            //convert JSON string to Map
           map = mapper.readValue(loginResponse.getBody(), new TypeReference<HashMap<String,Object>>(){});
        } catch (Exception e) {
             e.printStackTrace();
        }
        
        
        
        return map;
	}
}
