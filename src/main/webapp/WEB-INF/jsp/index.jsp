<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">

<head>
	<title>Single Sign On (SSO)</title>
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
  	
  	
  	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css">

        

        <!-- Favicon and touch icons -->
        
  	
  	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
</head>
<body>
	 <!-- Top content -->
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">                    
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        		
                        			<h4>Single Sign On (SSO) </h4>
                        			
                        			<c:if test="${param.error != null}">
							    		<p style="font-size: 20; color: #FF1C19;">Data yang anda masukkan salah, silahkan coba kembali.	</p>
							  	</c:if>
							  	<c:if test="${param.error == null}">
							  		<c:choose>
						  				<c:when test="${state != null && state == 'PRE_AUTH' && verify == null}">
						  					<c:choose>
						  						<c:when test="${resend != null}">
						  							<p>Kode OTP telah berhasil dikirim ulang ke email anda.</p>
						  						</c:when>
						  						<c:otherwise>
						  							<p>Kode OTP telah berhasil dikirim ke email anda.</p>
						  						</c:otherwise>
						  					</c:choose>
						  					
						  				</c:when>
						  				<c:otherwise>
						  					<c:choose>
						  					<c:when test="${(param.success != null || pageContext.request.userPrincipal != null) && verify == null}">
						  						<p>Login sukses, selamat datang!</p>
						  					</c:when>
						  					<c:otherwise>
					  							<p>Silahkan login dengan akun anda untuk masuk aplikasi</p>
					  						</c:otherwise>	
						  					</c:choose>
						  					
						  					
						  				</c:otherwise>
						  			</c:choose>
								  	
	                        		</c:if>	
                        		</div>
                        		<div class="form-top-right">
                        			<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
                        		</div>
                            </div>
                          <c:choose>
						  <c:when test="${(param.success != null || pageContext.request.userPrincipal != null) && verify == null}">
						  	<c:choose>
						  		<c:when test="${state != null && state == 'PRE_AUTH'}">
						  			<div class="form-bottom">
						  			<form role="form" action="${action}" method="post" class="login-form" name="f">
							  		<div class="form-group">
								  	<p><img class="img-circle" src="${pageContext.request.contextPath}/images/user.png" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;"><sec:authentication property="principal.fullName" /></span>
								    <input type="hidden" name="username" id="username" value="${userName}"> <a href="${pageContext.request.contextPath}/logout">(change)</a></span>
								     </div>
								    <input type="hidden" name="source" id="source" value="${token}">
		                				<div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" placeholder="Kode OTP" class="form-password form-control" id="password">
			                        </div>
				                      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				                      <button id="btnLogin" type="submit" class="btn">Masuk</button>
				                      
				                      <div class="form-group" style="margin-bottom:0px; text-align:right">
							  				<label>Belum terima kode, <a href="${pageContext.request.contextPath}/resend">Kirim ulang!</a></label>
							  			</div>
							  			
									</form>
									</div>
								</c:when>
								<c:otherwise>
									
								  <div class="form-bottom">
								  	<form class="login-form">
									    <p><img class="img-circle" src="${pageContext.request.contextPath}/images/user.png" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;"><sec:authentication property="principal.fullName" /></span>
									     </p>
									     <p> Anda mengelola sumber daya: 
									     <br><span style="font-family:arial; color:White; font-size: 18px;font-weight: bold;"><sec:authentication property="principal.source.name" /></span>
									     </p>
									    <button id="btnLogout" type="button" class="btn" onclick="location.href='logout'">Keluar</button>
									    
									    <div class="form-group" style="margin-bottom:0px; text-align:right">
							  				<label>Silahkan klik disini <a href="/app">masuk aplikasi</a></label>
							  			</div>
									</form>
								</div>
								</c:otherwise>
							</c:choose>
						  </c:when>
						  <c:otherwise>
				    			<div class="form-bottom">
				    			<c:choose>
						    <c:when test="${verify != null && verify == true}">
							  	<form role="form" action="${action}" method="post" class="login-form" name="f">
							  	<div class="form-group">
							  	<p><img class="img-circle" src="${pageContext.request.contextPath}/images/user.png" width="28px" /> <span style="font-family:arial; color:White; font-size: 18px;">${realName} </span>
							    <input type="hidden" name="username" id="username" value="${userName}"> <a href="${pageContext.request.contextPath}/refresh">(change)</a></span>
							     </div>
							    <c:choose>
				    				<c:when test="${listSrc != null}">
				    					<div class="form-group">
				    					<label class="sr-only" for="form-source">Sumber</label>
								    <select name="source" id="source" class="form-source form-control">
								    		<option value="">Pilih Sumber</option>
		                        			<c:forEach items="${listSrc}" var="item">
								    		<option value="${item.source.id}">${item.source.name}</option>
										</c:forEach>
									</select>
								    </div>
								</c:when>
				  				<c:otherwise>
				  					<input type="hidden" name="source" id="source" value="${source}">
				  				</c:otherwise>
	                				</c:choose>
	                				<div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" placeholder="Password" class="form-password form-control" id="password">
			                        </div>
			                        
			                        <div class="form-group" style="margin-bottom:0px;">
			                        <div class="checkbox">
									  <label><input type="checkbox" name="remember-me"> <span style="color:White;">Pertahankan login saya</span></label>
									</div>
			                        </div>
						  	</c:when>
						  	
						  	
						  	<c:otherwise>
						  		<form role="form" action="${pageContext.request.contextPath}/verify" method="post" class="login-form" name="f">
						  		<div class="form-group">
						  			<label class="sr-only" for="form-username">Username</label>
			                        	<input autocomplete="off" type="text" name="username" placeholder="Username atau Email" class="form-username form-control" id="username">
						  		</div>
						  	</c:otherwise>
			                 </c:choose>
			                    	
			                     <c:choose>
			                        
								  <c:when test="${param.logout != null}">
								    <button id="btnLogin" type="submit" class="btn">Masuk Kembali</button>
								  </c:when>
								  
								  <c:otherwise>
								    <button id="btnLogin" type="submit" class="btn">Masuk</button>
								  </c:otherwise>
								</c:choose>
			                        
			                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			                        
			                        <c:choose>
						    				<c:when test="${verify != null && verify == true}">
						    				
						    				<div class="form-group" style="margin-bottom:0px; text-align:right">
							  				<label>Lupa password, <a href="${pageContext.request.contextPath}/reset">klik disini!</a></label>
							  			</div>
				                        
							  			</c:when>
							  			<c:otherwise>
							  			
							  			<div class="form-group" style="margin-bottom:0px; text-align:right">
							  				<label>Belum punya akun, <a href="${pageContext.request.contextPath}/daftar">daftar disini!</a></label>
							  			</div>
							  			
							  			
							  			</c:otherwise>
							  		</c:choose>
			                    </form>
			                    
			                    
		                    </div>
				  </c:otherwise>
				</c:choose>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>

</body>
<script src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
<script src="${pageContext.request.contextPath}/js/login-script.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-migrate-1.3.0.js"></script>
<script>
console.log(Object.keys(jQuery.browser)[0]);
</script>

</html>