<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">

<head>
	<title>Lupa Password - Single Sign On (SSO)</title>
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
  	
  	
  	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css">

</head>
<body>
	 <!-- Top content -->
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">                    
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-6 col-sm-offset-3 form-box">
                        
                        
						<c:choose>                        
                        		<c:when test="${param.success != null}">
                        		
                        			<div class="form-top">
		                        		<div class="form-top-left">
		                        		
		                        			<h4>Single Sign On (SSO) </h4>
		                        			<p style="font-size: 20; color: #228B22;">Pendaftaran telah berhasil, silahkan login untuk masuk.</p>
		                        			
		                        		</div>
		                        		<div class="form-top-right">
		                        			<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
		                        		</div>
		                          </div>
						  		
						  		
						  		<div class="form-bottom">
							  	 <form role="form" action="${pageContext.request.contextPath}/verify" method="post" class="login-form" name="f">
							  	 	 <div class="form-group">
							  			<label class="sr-only" for="form-username">Username</label>
				                        	<input autocomplete="off" type="text" name="username" placeholder="Username atau Email" class="form-username form-control" id="username">
							  		</div>
					                  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					                  <button id="btnLogin" type="submit" class="btn">Masuk</button>
					                  
					                  
							  			<div class="form-group" style="margin-bottom:0px; text-align:right">
							  				<label>Belum punya akun, <a href="${pageContext.request.contextPath}/daftar">daftar disini!</a></label>
							  			</div>
							  			
								 </form>
							  </div>
						  	
						  	</c:when>
						  	<c:otherwise>
						  	
							  	<div class="form-top">
		                        		<div class="form-top-left">
		                        		
		                        			<h4>Pendaftaran Akun </h4>
		                        			
		                        			<c:choose>                        
                        					<c:when test="${error != null}">
                        						<p style="font-size: 20; color: #FF1C19;">Email yang anda masukkan sudah digunakan, coba yang lain.</p>
                        					</c:when>
                        					<c:otherwise>
                        						<p>Masukkan data anda untuk melakukan pendaftaran.</p>
                        					</c:otherwise>
                        					</c:choose>
		                        			
		                        		</div>
		                        		<div class="form-top-right">
		                        			<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
		                        		</div>
		                          </div>
							  	
							  	<div class="form-bottom">
							  	 <form role="form" action="${pageContext.request.contextPath}/daftar" method="post" class="login-form" name="f">
							  	 	 <div class="form-group">
							  			<label class="sr-only" for="form-realname">Nama</label>
			                        		<input value="${realname}" autocomplete="off" type="text" name="realname" placeholder="Nama sesuai identitas" class="form-realname form-control" id="realname">
							  		</div>
							  		<div class="form-group">
							  			<label class="sr-only" for="form-email">Email</label>
			                        		<input autocomplete="off" type="text" name="email" placeholder="Email yang masih aktif" class="form-email form-control" id="email">
							  		</div>
							  		<div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="password" placeholder="Password minimal 8 karakter" class="form-password form-control" id="password">
				                     </div>
					                  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					                  <button id="btnDaftar" type="submit" class="btn">${btnlabel}</button>
					                  
					                  <div class="form-group" style="margin-bottom:0px; text-align:right">
						  				<label>Sudah punya akun, <a href="${pageContext.request.contextPath}/">login disini!</a></label>
						  			</div>
								 </form>
							  	</div>
						  	
						  	</c:otherwise>
					    </c:choose>
                          
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>

</body>
<script src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
<script src="${pageContext.request.contextPath}/js/login-script.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-migrate-1.3.0.js"></script>
<script>
console.log(Object.keys(jQuery.browser)[0]);
</script>

</html>