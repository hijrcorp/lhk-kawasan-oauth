<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">

<head>
	<title>Lupa Password - Single Sign On (SSO)</title>
	<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
  	
  	
  	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-style.css">

</head>
<body>
	 <!-- Top content -->
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">                    
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-6 col-sm-offset-3 form-box">
                        
                        <c:choose>                        
                        		<c:when test="${param.update != null}">
                        		
                        		<div class="form-top">
	                        		<div class="form-top-left">
	                        		
	                        			<h4>Ubah Password </h4>
			                        	<p>Masukkan kode reset yang telah dikirim ke email anda.</p>
	                        			
	                        			
	                        		</div>
	                        		<div class="form-top-right">
	                        			<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
	                        		</div>
	                          </div>
	                          <div class="form-bottom">
	                          	<form role="form" action="${pageContext.request.contextPath}/update" method="post" class="login-form" name="f">
	                          		<input type="hidden" value="${param.key}" name="key" id="key" />
	                          		<div class="form-group">
								  		<label class="sr-only" for="form-username">Username</label>
			                        		<input value="${userId}" autocomplete="off" type="text" name="username" placeholder="Username atau Email" class="form-username form-control" id="username">
							  		</div>
							  		
							  		<div class="form-group">
				                        	<label class="sr-only" for="form-password">Password Baru</label>
				                        	<input type="password" name="password" placeholder="Masukkan Password Baru" class="form-password form-control" id="password">
			                        </div>
			                        
							  		<div class="form-group">
								  		<label class="sr-only" for="form-username">Kode Reset</label>
			                        		<input autocomplete="off" type="text" name="kode" placeholder="Kode Reset Password" class="form-kode form-control" id="kode">
							  		</div>
							  		
							  		
	                        			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	                        			<button id="btnUpdate" type="submit" class="btn">Simpan</button>
	                        			<div class="form-group" style="margin-bottom:0px; text-align:right">
						  				<label>Sudah punya akun, <a href="${pageContext.request.contextPath}/">login disini!</a></label>
						  			</div>
	                        		</form>	
	                        			
	                          </div>
						  	
						  	</c:when>
						  	<c:otherwise>
						  	
						  	<div class="form-top">
                        		<div class="form-top-left">
                        		
                        			<h4>Lupa Password </h4>
		                        	
                        			
                        			<c:choose>                        
                        				<c:when test="${param.error != null}">
                        					<p style="font-size: 20; color: #FF1C19;">Username atau email anda tidak terdaftar, silahkan coba kembali.</p>
                        				</c:when>
                        				<c:otherwise>
                        					<p>Masukkan username atau email anda yang terdaftar.</p>
                        				</c:otherwise>
                        			</c:choose>
							  	
                        		</div>
                        		<div class="form-top-right">
                        			<img id="logo" alt="hijr logo" src="${pageContext.request.contextPath}/images/logo.png" />
                        		</div>
                          </div>
                          <div class="form-bottom">
						  	 <form role="form" action="${pageContext.request.contextPath}/reset" method="post" class="login-form" name="f">
						  	 	 <div class="form-group">
							  		<label class="sr-only" for="form-username">Username</label>
		                        		<input value="${userId}" autocomplete="off" type="text" name="username" placeholder="Username atau Email" class="form-username form-control" id="username">
						  		</div>
						  		
				                  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				                  <button id="btnReset" type="submit" class="btn">Kirim</button>
				                  
				                  <div class="form-group" style="margin-bottom:0px; text-align:right">
						  				<label>Sudah punya akun, <a href="${pageContext.request.contextPath}/">login disini!</a></label>
						  			</div>
						  		
							 </form>
						  </div>
						  	
						  	</c:otherwise>
					    </c:choose>
                        
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>

</body>
<script src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
<script src="${pageContext.request.contextPath}/js/login-script.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-migrate-1.3.0.js"></script>

<script>
console.log(Object.keys(jQuery.browser)[0]);
</script>

</html>