
$( document ).ready(function() {

    /*
        Fullscreen background
    */
    $.backstretch(ctx + "/images/backgrounds/1.jpg");
    
    $("form").submit(function(e){
    		if($('[name=source]').length ){
    			$('[name=username]').val($('[name=username]').val()+'::'+$('[name=source]').val());
    		}
    		
    		if($('[name=realname]').length ){
    			if($('[name=realname]').val().length < 3){
    				alert('Nama minimal menggunakan 3 huruf.');
    				e.preventDefault();
    				return false;
    			}
    		}
    		
    		if($('[name=email]').length ){
			if(!validateEmail($('[name=email]').val())){
    				alert('Format email tidak benar, harap diperbaiki.');
    				e.preventDefault();
    				return false;
    			}
    		}
    		
    		if($('[name=password]').length && !$('#btnLogin').length ){
    			if($('[name=password]').val().length < 8){
    				alert('Password minimal 8 karakter kombinasi angka dan huruf.');
    				e.preventDefault();
    				return false;
    			}
    		}
    		
    		
    		$('.btn').text("Sedang proses..");
    });
    
    $('[name=source]').on('change', function(e){
    		$("[name=password]").focus();
    });
    
    if($('[name=realname]').length && $('[name=realname]').val().length == 0){
		$("[name=realname]").focus();
    }else if($('[name=email]').length){
    		$("[name=email]").focus();
    }else if($('[name=password]').length){
    		$("[name=password]").focus();
    }else{
    		$("[name=username]").focus();
    }
    
    
     
});


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
